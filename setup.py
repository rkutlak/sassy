"""
Usage:
    python3 setup.py py2app
"""

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
import io
import codecs
import os
import sys


APP = ['sassy/main.py']
DATA_FILES = []
OPTIONS = {
            'argv_emulation': True,
            'plist': 'sassy/resources/Info.plist'
          }

here = os.path.abspath(os.path.dirname(__file__))

def read(*filenames, **kwargs):
    encoding = kwargs.get('encoding', 'utf-8')
    sep = kwargs.get('sep', '\n')
    buf = []
    for filename in filenames:
        with io.open(filename, encoding=encoding) as f:
            buf.append(f.read())
    return sep.join(buf)

#long_description = read('README.txt', 'CHANGES.txt')
long_description = """
Demo of the Sassy project.

"""


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest
        errcode = pytest.main(self.test_args)
        sys.exit(errcode)

setup(
    name='SassyDemo',
    version=0.1,
    url='http://bitbucket.org/rkutlak/sassy',
    license='BSD',
    app=APP,
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    author='Roman Kutlak',
    install_requires=['rdflib>=3.0',
                      'pyyaml>=1.0',
                     ],
    tests_require=['pytest'],
    setup_requires=['py2app'],
    extras_require={
        'testing': ['pytest'],
    },
    cmdclass={'test': PyTest},
    author_email='roman@kutlak.net',
    description='Sassy Demo app.',
    long_description=long_description,
    packages=['sassy'],
    include_package_data=True,
    platforms='any',
    test_suite='sassy.test.main',
    classifiers = [
        'Programming Language :: Python',
        'Development Status :: 1 - Alpha',
        'Natural Language :: English',
#        'Environment :: Web Environment',
#        'Intended Audience :: Developers',
#        'License :: OSI Approved :: Apache Software License',
#        'Operating System :: OS Independent',
#        'Topic :: Software Development :: Libraries :: Python Modules',
#        'Topic :: Software Development :: Libraries :: Application Frameworks',
#        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        ]
)