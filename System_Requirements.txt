System Requirements

The Information Presentation part of the system has to be able to:
  - present a plan in a graphical form
  - present a plan in NL
  - present arguments for a plan in NL
[  - describe a state (e.g., sensory readings)]
  - allow the user to ask "why this plan?"
  - allow the user to ask "why this resource?"
  - allow the user to ask "why this action?"
  - allow the user to add knowledge (preferences, constraints)
  