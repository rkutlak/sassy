#!/usr/bin/python3
#
# This file is part of pyperplan.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#

from .parser_common import *
from .errors import *
from .tree_visitor import Visitable, TraversePDDLDomain, TraversePDDLProblem
from .lisp_parser import parse_lisp_iterator
from .lisp_parser import Token

"""
This module contains the main parser logic.
Partial parser for each AST node are implemented
and called recursively to construct a complete parse.
"""

# This emulates an c/c++ enum to distinguish between formulas variables and
# constants in formulas.
(TypeFormula, TypeVariable, TypeConstant) = range(3)

###
### Definitions of AST nodes
###


class Keyword(Visitable):
    """This class represents the AST node for a pddl keyword."""

    def __init__(self, name):
        """ Construct a new Keyword.

        Keyword arguments:
        name -- the name of the keyword e.g. 'typed' if the keyword
                were ':typed'
        """
        self._visitorName = 'visit_keyword'
        self.name = name


class Variable(Visitable):
    """ This class represents the AST node for a pddl variable."""

    def __init__(self, name, types=None):
        """ Construct a new Variable.

        Keyword arguments:
        name -- the name of the variable e.g. 'x' if the variable were '?x'
        types -- a list of names of Types denoting the possible types of this
                 variable
                 NOTE: checks that these types actually exist are implemented
                 in the TreeVisitor
        """
        self._visitorName = 'visit_variable'
        self.name = name
        assert(isinstance(self.name, str))
        self.typed = (types != None)  # either True or False
        self.types = types  # either None or a List of Types

class Type(Visitable):
    """This class represents the AST node for a pddl type."""

    def __init__(self, name, parent=None):
        """ Construct a new Type.

        Keyword arguments:
        name -- the name of the type
        parent -- a string that denotes the Typ instance that is the parent of
                  this type or None
        """
        self._visitorName = 'visit_type'
        self.name = name
        self.parent = parent  # either None or a Type


class Predicate(Visitable):
    """This class represents the AST node for a pddl predicate."""

    def __init__(self, name, parameters=None):
        """ Construct a new Predicate.

        Keyword arguments:
        name -- the name of the Predicate
        parameters -- a list of parameters described as variables
        """
        self._visitorName = 'visit_predicate'
        self.name = name
        self.parameters = parameters or []  # a list of Variables


class PredicateInstance(Visitable):
    """This class represents the AST node for a pddl predicate instance."""

    def __init__(self, name, parameters=[]):
        """ Construct a new Predicate.

        Keyword arguments:
        name -- the name of the Predicate
        parameters -- a list of parameters described as variables
        """
        self._visitorName = 'visit_predicate_instance'
        assert(isinstance(name, str))
        self.name = name
        self.parameters = parameters  # a list of object names

    def __repr__(self):
        return "<pddl.parser.PredicateInstance \"" + str(self.name) + \
               "\", parameters: " + str(self.parameters) + ">"

class RequirementsStmt(Visitable):
    """This class represents the AST node for a pddl requirements statement."""

    def __init__(self, keywords=None):
        """ Construct a new RequirementsStmt.

        Keyword arguments:
        keywords -- the list of requirements, represented as keywords
        """
        self._visitorName = 'visit_requirements_stmt'
        self.keywords = keywords or []  # a list of keywords


class DomainStmt(Visitable):
    """ This class represents the AST node for a pddl domain statement."""

    def __init__(self, name):
        """ Construct a new RequirementsStmt.

        Keyword arguments:
        name -- the domain name as a string
        """
        self._visitorName = 'visit_domain_stmt'
        self.name = name


class PreconditionStmt(Visitable):
    """This class represents the AST node for a pddl action precondition."""

    def __init__(self, formula):
        """ Construct a new PreconditionStmt.

        Keyword arguments:
        formula -- the parsed formula,
                   NOTE: Arbitrary formulas are allowed here. STRIPS
                   compatibility etc. is checked later by the TreeVisitor
        """
        self._visitorName = 'visit_precondition_stmt'
        self.formula = formula  # a Formula

    def __repr__(self):
        return "<pddl.parser.PreconditionStmt: " + str(self.formula) + ">"

class EffectStmt(Visitable):
    """This class represents the AST node for a pddl action effect."""

    def __init__(self, formula):
        """ Construct a new EffectStmt.

        Keyword arguments:
        formula -- the parsed formula,
                   NOTE: Arbitrary formulas are allowed here. STRIPS
                   compatibility etc. is checked later by the TreeVisitor
        """
        self._visitorName = 'visit_effect_stmt'
        self.formula = formula  # a Formula


class Formula(Visitable):
    """
    This class represents the AST node for a pddl formula,
    as it can be specified for preconditions and effects.
    """
    def __init__(self, key, children=None, formulaType=TypeFormula):
        """ Construct a new Formula.

        Keyword arguments:
        key -- the operator of the subformula e.g. 'not' if the formula were
               '(not (on a c))'
        children -- a list of immediate descending subformulas of this formula
        type -- the type of this formulas key --> one of
                (TypeFormula, TypeVariable, TypeConstant)
        """
        assert(isinstance(key, str) or isinstance(key, Variable))
        self._visitorName = 'visit_formula'
        self.key = key
        self.children = children or []  # a list of Formulas
        self.type = formulaType  # a Type

    def __repr__(self):
        t = "(" + str(self.key)
        if len(self.children) > 0:
            t = t + " ("
            firstchild = True
            for child in self.children:
                if firstchild:
                    firstchild = False
                else:
                    t = t  + ", "
                t = t + str(child)
            t = t + ")"
        t = t + ")"
        return t

class ActionStmt(Visitable):
    """This class represents the AST node for a pddl action."""

    def __init__(self, name, parameters, precond, effect):
        """ Construct a new Action.

        Keyword arguments:
        name -- the name of the action
        parameters -- a list of variables denoting the parameters
        precond -- the precondition of the action given as a Formula
        effect -- the effect of the action given as a Formula
        """
        self._visitorName = 'visit_action_stmt'
        self.name = name
        self.parameters = parameters  # a list of parameters
        self.precond = precond  # right now: a Formula << PreconditionStmt
        # right now also a Formula << EffectStmt
        # --> should be checked when traversing the tree
        self.effect = effect

    def __repr__(self):
        return "<parser.pddl.ActionStmt \"" + str(self.name) + \
               "\": parameters: " + \
               str(self.parameters) + ", precondition: " + \
               str(self.precond) + ">"

class PredicatesStmt(Visitable):
    """Represents the AST node for a pddl domain predicates definition."""

    def __init__(self, predicates):
        """ Construct a new Action.

        Keyword arguments:
        predicates -- a list of predicates
        """
        self._visitorName = 'visit_predicates_stmt'
        self.predicates = predicates  # a list of Predicates

    def __repr__(self):
        return "<PredicatesStmt: " + str(self.predicates) + ">"

class DomainDef(Visitable):
    """This class represents the AST node for a pddl domain."""

    def __init__(self, name, requirements=None, types=None, predicates=None,
                 actions=None, constants=None):
        """ Construct a new Domain AST node.

        Keyword arguments:
        name -- the domain name
        types -- a list of Type AST nodes
        predicates -- a list of Predicate AST nodes
        actions -- a list of Action AST nodes
        constants -- a list of Constants, as Object AST nodes
        """
        self._visitorName = 'visit_domain_def'
        self.name = name
        self.requirements = requirements  # a RequirementsStmt
        self.types = types  # a list of Types
        self.predicates = predicates  # a PredicatesStmt
        if actions == None:
            self.actions = []
        else:
            self.actions = actions  # a list of ActionStmt
        self.constants = constants

    def __repr__(self):
        t = "DomainDef<pddl.parser.DomainDef node:\n"
        t = t + "    requirements: " + str(self.requirements)
        t = t + ",\n    types: " + str(self.types)
        t = t + ",\n    actions: " + str(self.actions)
        t = t + ",\n    constants: " + str(self.constants)
        t = t + "\n>"
        return t

class ProblemDef(Visitable):
    """This class represents the AST node for a pddl domain."""

    def __init__(self, name, domainName, objects=None, init=None, goal=None):
        """ Construct a new Problem AST node.

            Keyword arguments:
            name -- the problem name
            domainName -- the domain name that corresponds to this problem
            objects -- a list of objects defined in the problem file
            init -- an initial condition represented by an InitStmt
            goal -- a  goal condition represented by an GoalStmt
        """
        self._visitorName = 'visit_problem_def'
        self.name = name
        assert(isinstance(domainName, str))
        self.domainName = domainName
        self.objects = objects
        self.init = init
        self.goal = goal

    def __repr__(self):
        t = "ProblemDef<pddl.parser.ProblemDef node:\n"
        t = t + "    name: " + str(self.name)
        t = t + ",\n    domain name: " + str(self.domainName)
        t = t + ",\n    objects: " + str(self.objects)
        t = t + ",\n    initial state: " + str(self.init)
        t = t + ",\n    goal: " + str(self.goal)
        t = t + "\n>"
        return t

class Object(Visitable):
    """This class represents the AST node for a pddl object."""

    def __init__(self, name, type):
        """ Construct a new Object AST node.

        Keyword arguments:
        name -- the name of the object
        type -- the name of this objects Type
        """
        self._visitorName = 'visit_object'
        self.name = name
        self.typeName = type

    def __repr__(self):
        return "<pddl.parser.Object \"" + self.name + "\", " + \
               "type name: " + str(self.typeName) + ">"

class InitStmt(Visitable):
    """
    This class represents the AST node for a pddl problem initial condition.
    """
    def __init__(self, predicates):
        """ Construct a new InitStmt AST node.

        Keyword arguments:
        predicates -- a list of predicates denoting the initial codition
        """
        self._visitorName = 'visit_init_stmt'
        self.predicates = predicates

    def __repr__(self):
        return "<pddl.parser.InitStmt " + str(self.predicates) + ">"

class GoalStmt(Visitable):
    """This class represents the AST node for a pddl problem goal condition."""

    def __init__(self, formula):
        """ Construct a new GoalStmt AST node.

        Keyword arguments:
        predicates -- a list of predicates denoting the goal codition
        """
        self._visitorName = 'visit_goal_stmt'
        self.formula = formula

    def __repr__(self):
        return "<pddl.parser.GoalStmt " + str(self.formula) + ">"

###
### some little helper functions
###


def parse_name(iter, father):
    if not iter.peek().is_word():
        raise ValueError('Error %s predicate statement must contain a name!' %
                         father)
    return next(iter).get_word()


def parse_list_template(f, iter):
    """ This function implements a common pattern used in this parser.

    It tries to parse a list of 'f' objects from the string 'string[i:end]'.
    The 'f' objects must be seperated by whitespace
    Returns a tuple of the position after the parsed list and the list.
    """
    result = list()
    # parse all possible occurences up to the end of the substring
    for elem in iter:
        var = f(elem)
        if var != None:
            result.append(var)
    return result


def _parse_string_helper(iter):
    return iter.get_word().name


def _error_pos(iter):
    if iter == None or not hasattr(iter, "lineNumber"):
        return "Error in unknown line: "
    return "Error in line " + str(iter.lineNumber) + ", column " + \
           str(iter.colNumber) + ": "

def _parse_type_helper(iter, type_class):
    """ This function implements another common idiom used in this parser.

    It parses a list consisting either of Objects or Variables or Types
    which can all have additional type inheritance information.
    A list of objects could for example be defined as:
    o1 o2 o3 o4 - car
    Which would represent 4 objects (o1-o4) of type car.
    Since Variable- and Typelists are specified using the same pattern for
    type/supertype information this function takes the 'type_class' as an
    argument and parses an appropriate list of type_class instances.

    Returns the parsed list of instances.
    """
    # there may be several objects with the same type
    # hence we need to store each parsed object in a list and attach a new type
    # instance whenever a type is specified
    result = list()
    tmpList = list()
    while not iter.empty():
        var = next(iter).get_word()
        if var == None or var.name == None:
            continue
        if type_class != Variable and len(var.name) > 0 and \
                var.name[0] in reserved:
            raise ValueError(_error_pos(var.lineNumber) + \
                             'type must not begin with reserved char!')
        elif var.name[0] == '-':
            # check if either definition present
            if iter.peek().is_structure():
                # must contain either definition
                types_iter = next(iter)
                if not types_iter.try_match('either'):
                    raise ValueError(_error_pos(types_iter) + \
                                     'multiple parent definition must '
                                     'start with "either"')
                tlist = parse_list_template(_parse_string_helper, types_iter)
                while len(tmpList) != 0:
                    #for token in tlist:
                    #    assert(not isinstance(token, Token))
                    result.append(type_class(tmpList.pop(), tlist))
            else:
                # found type information --> flush objects into result list
                ctype = next(iter).get_word()
                while len(tmpList) != 0:
                    if type_class == Variable:
                        result.append(type_class(tmpList.pop(),
                                                 [ctype.name]))
                    else:
                        result.append(type_class(tmpList.pop(),
                                      ctype.name))
        elif var.name != '':
            # found new object definition --> enqueue
            if type_class == Variable:
                if var.name[0] != '?':
                    raise ValueError(_error_pos(var) + \
                                     'variables must start with a "?": ' + \
                                     'got "' + var.name + '"')
                tmpList.insert(0, var.name)
            else:
                tmpList.insert(0, var.name)
    while len(tmpList) != 0:
        # append all left over objects --> these are untyped !!
        result.append(type_class(tmpList.pop(), None))
    return result


###
### parser functions
###

def parse_keyword(iter):
    """ Parses a keyword from a given substring string[i:end].
        Returns the position in the string after the parsed keyword
        and the keyword itself as a tuple.
    """
    name = iter.get_word()
    if name.name == '':
        raise ValueError(_error_pos(name) + \
                         'empty keyword found')
    # ensure keyword starts with ':'
    if name.name[0] != ':':
        raise ValueError(_error_pos(name) + \
                         'keywords have to start with a colon ":"')
    return Keyword(name.name[1:])


def parse_keyword_list(iter):
    """Parses a list of keywords using the parse_list_template helper.

    Returns a tuple of the position within the string after the parsed list and
    the list itself.
    """
    return parse_list_template(parse_keyword, iter)


def parse_variable(iter):
    """Parses a Variable from the supplied string.

    Returns the position after the variable definition and a Variable instance.
    """
    name = iter.get_word()
    if name.name == '':
        raise ValueError(_error_pos(name) + 'empty variable found')
    # ensure variable starts with '?'
    if name.name[0] != '?':
        raise ValueError(_error_pos(name) + 'variables must start with a "?"')
    return Variable(name.name, None)


def parse_typed_var_list(iter):
    """
    Parses a list of - possibly typed - variables using the _parse_type_helper
    function.

    Returns the position after the type list and the resulting list of type
    instances.
    """
    return _parse_type_helper(iter, Variable)


def parse_parameters(iter, name):
    """
    Parses a list of parameters using the parse_typed_var_list parser function.
    """
    # check that the parameters definition starts with the correct keyword
    if not iter.try_match(':parameters'):
        raise ValueError(_error_pos(iter) +
                         'keyword ":parameters" expected in action "' + \
                         name + '"')
    varList = parse_typed_var_list(next(iter))
    return varList


def parse_requirements_stmt(iter):
    """ Parse the pddl requirements definition.
        Returns an RequirementsStmt.
    """
    # check for requirements keyword
    if not iter.try_match(':requirements'):
        raise ValueError(_error_pos(iter) +
                         'keyword ":requirements" expected')
    keywords = parse_keyword_list(iter)
    return RequirementsStmt(keywords)


def _parse_types_with_error(iter, keyword, classt):
    if not iter.try_match(keyword):
        raise ValueError('Error keyword "%s" required before %s!' %
                         (keyword, classt.__name__))
    return _parse_type_helper(iter, classt)


# Constants / Objects and types can be parsed in the same way because of their
# familiar structure.
# Hence instantiate them with _parse_types_with_error.
_common_types = [(':types', Type), (':objects', Object),
                 (':constants', Object)]
(parse_types_stmt, parse_objects_stmt, parse_constants_stmt) = \
    map(lambda tup: lambda it: _parse_types_with_error(it, tup[0], tup[1]),
                                                       _common_types)


def _parse_domain_helper(iter, keyword):
    """Parses the domain statement, which consists of the domain name.

    Returns a DomainStmt instance.
    """
    if not iter.try_match(keyword):
        raise ValueError('Error domain statement must be present before '
                         'domain name!')
    name = parse_name(iter, 'domain')
    return DomainStmt(name.name)

parse_domain_stmt = lambda it: _parse_domain_helper(it, 'domain')
parse_problem_domain_stmt = lambda it: _parse_domain_helper(it, ':domain')


def parse_predicate(iter):
    """
    Parse a single predicate instance by parsing its name and a list of typed
    variables defining the signature.
    Returns a Predicate instance.
    """
    name = parse_name(iter, 'predicate')
    params = parse_typed_var_list(iter)
    return Predicate(name.name, params)


def parse_predicate_list(iter):
    """Parses a list of predicates using the parse_list_template helper.

    Returns a list containing predicates.
    """
    return parse_list_template(parse_predicate, iter)


def parse_predicate_instance(iter):
    """
    Parses a predicate instance which is a predicate with possibly instantiated
    signature.
    Returns a Predicate instance.
    """
    name = parse_name(iter, 'domain')
    params = parse_list_template(_parse_string_helper, iter)
    return PredicateInstance(name.name, params)


def parse_predicate_instance_list(iter):
    """
    Parse a list of predicate instances using the parse_list_template helper.
    """
    return parse_list_template(parse_predicate_instance, iter)


def parse_formula(iter):
    """Parse a Formula recursively

    Note: This parses formulas recursively !!
          We do not use tail recursion

    Returns the position after the formula and the Formula instance
    """
    if iter.is_structure():
        # this is a nested formula
        type = TypeFormula
        if iter.peek() != None:
            key = iter.peek().get_word()
        else:
            # empty formula
            return None
        next(iter)
        if key.name[0] in reserved:
            raise ValueError('Error in line ' + str(iter.lineNumber) + \
                             ': formula must not start with reserved '
                             'char!')
        children = parse_list_template(parse_formula, iter)
        key = key.name
    else:
        # non nested formula
        key = iter.get_word()
        children = []
        if key.name[0] == '?':
            key = parse_variable(iter)
            type = TypeVariable
        else:
            key = key.name
            type = TypeConstant
    return Formula(key, children, type)


def _parse_precondition_or_effect(iter, keyword, type):
    """Parse an action precondition or effect

    Returns a PreconditionStmt or EffectStmt instance.
    """
    if not iter.try_match(keyword):
        raise ValueError('Error in line ' + str(iter.lineNumber) + \
                         ': %s must start with "%s" keyword' %
                         (type.__name__, keyword))
    cond = parse_formula(next(iter))
    return type(cond)


def parse_precondition_stmt(it):
    return _parse_precondition_or_effect(it, ':precondition',
        PreconditionStmt)


def parse_effect_stmt(it):
    return _parse_precondition_or_effect(it, ':effect', EffectStmt)


def parse_action_stmt(iter):
    """
    Parse an action definition which consists of a name, parameters,
    an optional precondition and an effect.

    Returns an ActionStmt instance.
    """
    origiter = iter
    # each action begins with a name
    if not iter.try_match(':action'):
        raise ValueError(_error_pos(origiter) + \
                         'action must start with ":action" keyword!')
    name = parse_name(iter, 'action').name
    # call parsers to parse parameters, precondition, effect
    param = parse_parameters(iter, name)
    pre = None
    eff = None
    # try to parse precondition and effect in any order:
    while True:
        matched = False
        # check if this is a precondition statement:
        if pre == None:
            if iter.try_match(":precondition", True):
                pre = parse_precondition_stmt(iter)
                matched = True
                continue
        # check if this is an effect statement:
        if eff == None:
            if iter.try_match(":effect", True):
                eff = parse_effect_stmt(iter)
                matched = True
                continue
        # if this matched nothing, stop looking further:
        if not matched:
            peeked = iter.peek()
            if peeked != None and peeked.is_word():
                if peeked.get_word().name[0] == ":":
                    raise ValueError(_error_pos(origiter) + \
                                     'found invalid keyword while parsing ' \
                                     'action "' + name + '": "' + \
                                     peeked.get_word().name + '"')
            break

    # if the effect is missing raise an error:
    if eff == None:
        raise ValueError(_error_pos(origiter) +
                         'effect statement missing from action "' +
                         name + '"')
    return ActionStmt(name, param, pre, eff)

def parse_predicates_stmt(iter):
    """
    Parse a PredicatesStmt which is essentially a list of predicates preceded
    by the ':predicates' keyword.

    Returns a PredicatesStmt instance
    """
    origiter = iter
    if not iter.try_match(':predicates'):
        raise ValueError(_error_pos(origiter) + \
                         'predicate definition must start with ' + \
                         '":predicates" keyword!')
    preds = parse_predicate_list(iter)
    return PredicatesStmt(preds)


def parse_domain_def(iter):
    """Main parser method to parse a domain definition.

    Recursively calls all parsers needed to parse a domain definition.
    Returns a DomainDef instance
    """
    defString = parse_name(iter, 'domain def')
    if defString.name != 'define':
        raise ValueError('Error in line ' + str(defString.lineNumber) + ': ' \
                         + 'invalid domain definition! --> domain ' \
                         + 'definition must start with "define"')
    dom = parse_domain_stmt(next(iter))
    # create new DomainDef
    domain = DomainDef(dom.name)
    # first parse all optional keywords
    while not iter.empty():
        origiter = iter
        next_iter = next(iter)
        origiter = next_iter.peek()
        key = parse_keyword(next_iter.peek())
        if key.name == 'requirements':
            req = parse_requirements_stmt(next_iter)
            domain.requirements = req
        elif key.name == 'types':
            types = parse_types_stmt(next_iter)
            domain.types = types
        elif key.name == 'predicates':
            pred = parse_predicates_stmt(next_iter)
            domain.predicates = pred
        elif key.name == 'constants':
            const = parse_constants_stmt(next_iter)
            domain.constants = const
        elif key.name == 'action':
            action = parse_action_stmt(next_iter)
            domain.actions.append(action)
            # from this point on only actions are allowed to follow
            break
        else:
            raise ValueError(_error_pos(origiter) + \
                             'found unknown keyword in domain definition: "' \
                             + key.name + '"')
    # next parse all defined actions
    while not iter.empty():
        origiter = iter
        next_iter = next(iter)
        key = parse_keyword(next_iter.peek())
        if key.name != 'action':
            raise ValueError(_error_pos(origiter) + \
                             'found invalid keyword while parsing ' \
                             'actions: "' + key.name + '"')
        action = parse_action_stmt(next_iter)
        domain.actions.append(action)
    # assert end is reached
    iter.match_end()
    return domain


def parse_problem_name(iter):
    """
    Parse a problem name, which is a string, preceded by the ':problem'
    keyword.

    Returns the name as a string.
    """
    if not iter.try_match('problem'):
        raise ValueError('Invalid problem name specification! problem name '
                         'definition must start with "problem"')
    name = parse_name(iter, 'problem name')
    return name


def parse_problem_def(iter):
    """Main method to parse a problem definition.

    All parser metthods that are needed to parse a problem are called
    recursively by this function.

    Returns a ProblemDef instance
    """
    if not iter.try_match('define'):
        raise ValueError('Invalid problem definition! --> problem definition '
                         'must start with "define"')
    # parse problem name and corresponding domain name
    probname = parse_problem_name(next(iter))
    dom = parse_problem_domain_stmt(next(iter))
    # parse all object definitions
    objects = dict()
    if iter.peek_tag().name == ':objects':
        objects = parse_objects_stmt(next(iter))
    init = parse_init_stmt(next(iter))
    goal = parse_goal_stmt(next(iter))
    # assert end is reached
    iter.match_end()
    # create new ProblemDef instance
    return ProblemDef(probname.name, dom.name, objects, init, goal)


def parse_init_stmt(iter):
    """Parse the init statement of a problem definition.

    The InitStmt consists of a list of predicates and thus uses
    parse_predicate_instance_list.

    Returns an InitStmt instance.
    """
    origiter = iter
    if not iter.try_match(':init'):
        raise ValueError(_error_pos(origiter) + \
                'found invalid keyword when parsing InitStmt: ' + \
                str(origiter.peek()))
    preds = parse_predicate_instance_list(iter)
    return InitStmt(preds)


def parse_goal_stmt(iter):
    """Parse the init statement of a problem definition.

    The InitStmt consists of an arbitrary formula (STRIPS semantic will be
    checked later by the tree visitor).

    Returns an GoalStmt instance.
    """
    if not iter.try_match(':goal'):
        raise ValueError('Error found invalid keyword when parsing GoalStmt')
    f = parse_formula(next(iter))
    return GoalStmt(f)


class Parser(object):
    """
    This is the main Parser class that can be used from outside this module
    to translate a given domain and problem file into the representation given
    in pddl.py!
    """
    def __init__(self, domFile, probFile=None):
        """Constructor

        Keyword arguments:
        domFile -- the domain File
        probFile -- the problem File or None
        """
        self.domFile = domFile
        self.probFile = probFile
        self.domInput = ''
        self.probInput = ''

    def _read_input(self, source):
        """Reads the lisp input from a given source and normalizes it.

        Returns the LispIterator that is read from the source.
        """
        result = parse_lisp_iterator(source)
        return result

    def parse_domain(self, domFile=None, read_from_file=True):
        """
        Method that parses a domain, this method will be called from outside
        the parser.

        Keyword arguments:
        domFile        -- the domain to be parsed. If None, the one supplied
                          through the constructor will be used.
        read_from_file -- defines whether the input should be read from a file
                          (domFile is a file name)
                          or directly from the input string
                          (domFile contains domain PDDL). Default: True
        """
        if domFile is not None:
            self.domFile = domFile
        if not isinstance(domFile, str) and not domFile is None:
            raise TypeError("domFile needs to be a string (either file " +
                "name or domain PDDL)")
        if read_from_file:
            print("domFile: " + self.domFile)
            with open(self.domFile, encoding='utf-8') as file:
                self.domInput = self._read_input(file)
        else:
            self.domInput = self.domFile
            self.probInput = self.probFile
            assert(len(self.domInput.strip(' \t\n\r')) > 0)
            input = self.domInput.split('\n')
            self.domInput = self._read_input(input)
        domAST = parse_domain_def(self.domInput)
        # initialize the translation visitor
        visitor = TraversePDDLDomain()
        # and traverse the AST
        domAST.accept(visitor)
        # finally return the pddl.Domain
        self.last_parsed_domain = visitor.domain
        return visitor.domain

    def parse_problem(self, domain=None, probFile=None, read_from_file=True):
        """
        Method that parses a problem, this method will be called from outside
        the parser.

        Keyword arguments:
        domain         -- the domain to be used. If none is specified,
                          the one last parsed by parse_domain() will be used.
        probFile       -- the problem to be used. If none is specified,
                          the one specified in the constructor will be used.
        read_from_file -- defines whether the input should be read from a file
                          or directly from the input string. default: True
        """
        if probFile is not None:
            self.probFile = probFile
        if read_from_file:
            with open(self.probFile, encoding='utf-8') as file:
                self.probInput = self._read_input(file)
        else:
            self.probInput = self.probFile
            input = self.probInput.split('\n')
            self.probInput = self._read_input(input)
        probAST = parse_problem_def(self.probInput)
        # if no domain was provided, take the one we parsed last:
        if domain == None:
            domain = self.last_parsed_domain
        # initialize the translation visitor
        visitor = TraversePDDLProblem(domain)
        # and traverse the AST
        probAST.accept(visitor)
        # finally return the pddl.Problem
        return visitor.get_problem()

    # some setter and getter functions
    def set_domain_file(self, fname):
        self.domFile = fname

    def set_prob_file(self, fname):
        self.probFile = fname

    def get_domain_file(self):
        return self.domFile

    def get_prob_file(self):
        return self.probFile


if __name__ == '__main__':
    # additional imports here to prevent cyclic imports!
    argparser = argparse.ArgumentParser()
    argparser.add_argument(dest='domain', help='specify domain file')
    argparser.add_argument(dest='problem', help='specify problem file',
                           nargs='?')
    options = argparser.parse_args()
    if options.domain == None:
        parser.print_usage()
        parser.error('Error domain file must be specified')
    pddlParser = Parser(options.domain)
    print('-------- Starting to parse supplied domain file!')
    domain = pddlParser.parse_domain()
    print('++++++++ parsed domain file successfully')
    print(domain)
    if options.problem != None:
        print('-------- Starting to parse supplied problem file!')
        pddlParser.set_prob_file(options.problem)
        problem = pddlParser.parse_problem(domain)
        print('++++++++ parsed problem file successfully')
        print(problem)
