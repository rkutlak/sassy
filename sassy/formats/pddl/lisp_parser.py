#
# This file is part of pyperplan.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#

"""Basic functions for parsing simple Lisp files."""


from .errors import ParseError
from .lisp_iterators import LispIterator


def parse_lisp_iterator(input):
    return LispIterator(parse_nested_list(input))


def parse_nested_list(input_file):
    tokens = _tokenize(input_file)
    #for token in tokens:
    #    print("DEBUG: TOKEN IS: " + str(token.name) + " (line " + \
    #        str(token.lineNumber) + ")")
    next_token = next(tokens)
    if next_token.name != "(":
        raise ParseError("Error in line " + str(next_token.lineNumber) + \
             ", column " + str(next_token.colNumber) + \
             ": expected '(', got %s." % next_token.name)
    result = list(_parse_list_aux(tokens))
    for tok in tokens:  # Check that generator is exhausted.
        raise ParseError("Error in line " + str(tok.lineNumber) + ", " + \
            "column " + str(tok.colNumber) + \
            ": unexpected token: %s." % tok.name)
    return result

class Token:
    def __init__(self, name, lineNumber, colNumber):
        self.name = name.lower()
        self.lineNumber = lineNumber
        self.colNumber = colNumber

    def __repr__(self):
        return "<pddl.lisp_parser.Token \"" + self.name + "\" at " + \
               "line " + str(self.lineNumber) + ", col: " + \
               str(self.colNumber) + ">"

def _tokenize(input_file):
    lineNumber = 1
    for line in input_file:
        line = line.partition(";")[0]  # Strip comments.
        # Add whitespace at the end that terminates our final token:
        line = line + " "
        # Walk through tokens:
        # (we do this char per char so we know the exact column we're at)
        i = 0
        tokenstart = 0
        while i < len(line):
            if line[i] == "(" or line[i] == ")" or line[i] == '?' \
            or line[i] == ' ':
                # yield the current token (if any):
                currentToken = line[tokenstart:i].strip(" \t\r\n")
                if len(currentToken) > 0:
                    yield Token(currentToken, lineNumber, i)
                # set new token start:
                if line[i] == '?':
                    # include the question mark into next token
                    tokenstart = i
                else:
                    tokenstart = i + 1
                if line[i] == "(" or line[i] == ")":
                    # yield bracket as separate tokens:
                    yield Token(line[i], lineNumber, i)
            i = i + 1
        lineNumber = lineNumber + 1


def _parse_list_aux(tokenstream):
    # Invariant: leading "(" has already been swallowed.
    lastLineNumber = -1
    lastColNumber = -1
    for token in tokenstream:
        lastLineNumber = token.lineNumber
        lastColNumber = token.colNumber
        if token.name == ")":  # List is closed.
            return
        elif token.name == "(":  # Recursive call.
            yield list(_parse_list_aux(tokenstream))
        else:
            yield token
    # If we exhausted the stream, the list is unbalanced.
    raise ParseError("Error in unknown line close to line " + \
        str(lastLineNumber) + ", column: " + str(lastColNumber) + \
        ": missing closing parenthesis")

