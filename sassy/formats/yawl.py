#
# This file is part of SAsSy
#
# @author Roman Kutlak
#
# The YAWL files are XML so I parse them using the xml.sax parser.
# To avoid maintaining too many states, each class can parse a specific
# element. When a lower level element is encountered, an instance of a class
# for that elemen is set as the parser's content handler. When a closing
# element is encountered, the current instance sets it's parent as
# a new content handler (returning the control).


from xml import sax
from formats.strips import parse_plan_for_yawl

# use libxml2 if we can as it is faster, else use the default implementation
try:
    import lxml.etree as ET
except ImportError:
    import xml.etree.ElementTree as ET


""" This module serves as a STRIPS plan to YAWL workflow converter.

    The write_yawl_plan already does that, but uses string concatenation to create
    the XML. Using python's etree might be a neater way of doing it.

    """


def element(parent, name, text=None, attrs=None):
    """ Create a new ET.SubElement with given text and attributes. Helper fn."""
    elt = ET.SubElement(parent, name)
    if text is not None:
        elt.text = text
    if attrs is not None:
        for k, v in attrs.items():
            elt.set(k, v)
    return elt


def create_header(creator='', description='', version=0.1, identifier=''):
    header = """
<?xml version="1.0" encoding="UTF-8"?>
<specificationSet xmlns="http://www.yawlfoundation.org/yawlschema" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.2" xsi:schemaLocation="http://www.yawlfoundation.org/yawlschema http://www.yawlfoundation.org/yawlschema/YAWL_Schema2.2.xsd">
<specification uri="transportFlat">
<metaData>
<creator>%s</creator>
<description>%s</description>
<version>%d</version>
<persistent>false</persistent>
<identifier>%s</identifier>
</metaData>
    """
    header = (header % (creator, description, version, identifier))
    return header


def create_root(domain='', creator='', description='',
                version=0.1, identifier=''):
    root = ET.Element('specificationSet')
    root.set('xmlns', 'http://www.yawlfoundation.org/yawlschema')
    root.set('xmlns:xs', 'http://www.w3.org/2001/XMLSchema')
    root.set('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
    root.set('version', '2.2')
    root.set('xsi:schemaLocation', ('http://www.yawlfoundation.org/yawlschema' +
        ' http://www.yawlfoundation.org/yawlschema/YAWL_Schema2.2.xsd'))

    specification = element(root, 'specification', attrs={'uri', domain})

    md = element(specification, 'metaData')

    creator = element(md, 'creator', text=creator)
    description = element(md, 'description', text=description)
    version = element(md, 'version', text=version)
    
    tree = ET.ElementTree(root)
    tree.write("filename.xml")


def parse_yawl_file(file):
    """Function for parsing YAWL formatted file.

    The function constructs a parser, parses the file and returns the data 
    wrapped in a YawlContentHandler instance.
    """
    parser = sax.make_parser()
    data = YawlContentHandler(parser)
    parser.setContentHandler(data)
    parser.parse(open(file, 'r'))
    return data


DEBUG = False

class YawlContentHandler(sax.ContentHandler):
    """YAWL parser and content handler.
    
    The class parses a YAWL formatted file or string and stores ths parsed
    data inside.
    """
    def __init__(self, parser):
        self.parser = parser
        self.tasks = dict()
        self.decs = dict()
        self.name = ''
        self.description = ''
        self.identifier = ''
        self.creator = ''
        self.version = ''
        self.domain = ''

        self.reading_tasks = False
        self.reading_metadata = False

        self.ignore_layout = False
        self.tag = None

    def startElement(self, name, attrs):
        self.tag = name

        if self.ignore_layout:
            pass

        elif 'layout' == name:
            self.ignore_layout = True

        elif 'specification' == name:
            self.domain = attrs['uri']

        elif 'metaData' == name:
            self.reading_metadata = True

        elif 'decomposition' == name:
            if 'isRootNet' in attrs:
                self.reading_tasks = True
                self.name = attrs['id']
            else:
                self.reading_tasks = False
                d = Decomposition(self.parser, self)
                id = attrs['id']
                self.decs[id] = d
                self.parser.setContentHandler(d)
                if DEBUG: print ("decomposition id: %s" % id)

        elif 'task' == name and self.reading_tasks:
            id = attrs['id']
            t = Task(self.parser, self, id)
            self.tasks[id] = t
            self.parser.setContentHandler(t)
            if DEBUG: print ("task id: %s" % id)

        elif 'condition' == name and self.reading_tasks:
            c = Condition(self.parser, self)
            id = attrs['id']
            c.id = id
            self.tasks[id] = c
            self.parser.setContentHandler(c)
            if DEBUG: print ("condition id: %s" % id)

        elif 'inputCondition' == name:
            c = InputCondition(self.parser, self)
            id = attrs['id']
            c.id = id
            self.tasks[id] = c
            self.parser.setContentHandler(c)
            if DEBUG: print ("input condition id: %s" % id)

        elif 'outputCondition' == name:
            c = OutputCondition(self.parser, self)
            id = attrs['id']
            c.id = id
            self.tasks[id] = c
            self.parser.setContentHandler(c)
            if DEBUG: print ("output condition id: %s" % id)

    def endElement(self, name):
        self.tag = None

        if self.ignore_layout:
            pass
        elif 'layout' == name:
            self.ignore_layout = False
        elif 'metaData' == name:
            self.reading_metadata = False

    def characters(self, data):
        if 'description' == self.tag:
            self.description += data
        elif 'identifier' == self.tag:
            self.identifier += data
        elif 'creator' == self.tag:
            self.creator += data
        elif 'version' == self.tag:
            self.version += data


class Task(sax.ContentHandler):
    def __init__(self, parser, parent, id=''):
        # housekeeping
        self.parser = parser
        self.parent = parent
        # data
        self.id = id
        self.name = ''
        self.flows_into = list()
        self.decomposition_id = None
        self.split = None
        self.join = None
        # this might not be necessary
        self.is_input_condition = False
        self.is_output_condition = False
        self.is_condition = False
        # state keeping vars
        self.reading_name = False
        self.reading_flows = False

    def startElement(self, name, attrs):
        if 'name' == name:
            self.reading_name = True
        elif 'decomposesTo' == name:
            self.decomposition_id = attrs['id']
        elif 'flowsInto' == name:
            self.reading_flows = True
        elif 'nextElementRef' == name and self.reading_flows:
            self.flows_into.append(attrs['id'])
        elif 'join' == name:
            self.join = attrs['code']
        elif 'split' == name:
            self.split = attrs['code']

    def endElement(self, name):
        if 'name' == name:
            self.reading_name = False
        
        elif 'flowsInto' == name:
            self.reading_flows = False
        
        elif 'task' == name:
            self.parser.setContentHandler(self.parent)
    
    def characters(self, data):
        if self.reading_name :
            self.name += data


class InputCondition(Task):
    def __init__(self, parser, parent):
        Task.__init__(self, parser, parent)
        self.is_input_condition = True
    
    def endElement(self, name):
        if 'name' == name:
            self.reading_name = False
        
        elif 'flowsInto' == name:
            self.reading_flows = False

        elif 'inputCondition' == name:
            if self.name is None or self.name == '':
                self.name = 'inputCondition'
            self.parser.setContentHandler(self.parent)
            if DEBUG: print ("parsed input condition with %d outflows" %
                   len(self.flows_into))


class Condition(Task):
    def __init__(self, parser, parent):
        Task.__init__(self, parser, parent)
        self.is_condition = True
    
    def endElement(self, name):
        if 'name' == name:
            self.reading_name = False
        
        elif 'flowsInto' == name:
            self.reading_flows = False

        elif 'condition' == name:
            if self.name is None or self.name == '':
                self.name = 'outputCondition'
            self.parser.setContentHandler(self.parent)
            if DEBUG: print ("parsed condition with %d outflows" %
                   len(self.flows_into))


class OutputCondition(Task):
    def __init__(self, parser, parent):
        Task.__init__(self, parser, parent)
        self.is_output_condition = True
    
    def endElement(self, name):
        if 'name' == name:
            self.reading_name = False
        
        elif 'flowsInto' == name:
            self.reading_flows = False

        elif 'outputCondition' == name:
            self.parser.setContentHandler(self.parent)
            if DEBUG: print ("parsed output condition '%s'" % self.name)


class Decomposition(sax.ContentHandler):
    def __init__(self, parser, parent):
        self.parser = parser
        self.parent = parent
        self.input_params = list()
        self.output_params = list()

    def startElement(self, name, attrs):
        if 'inputParam' == name:
            param = InputParam(self.parser, self)
            self.parser.setContentHandler(param)
            self.input_params.append(param)

        elif 'outputParam' == name:
            param = OutputParam(self.parser, self)
            self.parser.setContentHandler(param)
            self.output_params.append(param)

    def endElement(self, name):
        if 'decomposition' == name:
            self.parser.setContentHandler(self.parent)


class InputParam(sax.ContentHandler):
    def __init__(self, parser, parent):
        self.parser = parser
        self.parent = parent
        self.tag = None
        self.name = ''
        self.value = ''
    
    def startElement(self, name, attrs):
        self.tag = name
        if 'name' == name:
            self.reading_name = True
        elif 'defaultValue' == name:
            self.reading_value = True
    
    def endElement(self, name):
        self.tag = None
        if 'inputParam' == name:
            self.parser.setContentHandler(self.parent)
    
    def characters(self, data):
        if 'name' == self.tag:
            self.name += data
        elif 'defaultValue' == self.tag:
            self.value += data


class OutputParam(InputParam):
    def endElement(self, name):
        if 'name' == name:
            self.reading_name = False
        elif 'defaultValue' == name:
            self.reading_value = False
        elif 'outputParam' == name:
            self.parser.setContentHandler(self.parent)


# Nava's code for generating YAWL files form STRIPS plan

''' TODO: modifier for header, i.e. add author etc to metadata'''
# Returns canned XML header
def add_header():
    header = """<?xml version="1.0" encoding="UTF-8"?>
        <specificationSet xmlns="http://www.yawlfoundation.org/yawlschema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.2" xsi:schemaLocation="http://www.yawlfoundation.org/yawlschema http://www.yawlfoundation.org/yawlschema/YAWL_Schema2.2.xsd">
        <specification uri="transportFlat">
        <metaData>
        <creator>nava</creator>
        <description>This is a test file.</description>
        <version>0.7</version>
        <persistent>false</persistent>
        <identifier>UID_b5bacc06-a376-448d-9877-2f494aaca2a9</identifier>
        </metaData>
        <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" />
        """
    return header

# Given a list of inputs generates the XML for the net decomposition
# Needs to be preceded with net_decomposition
def add_net_input(all_inputs):

    index = 0
    parameter_string=""

    for input in all_inputs:
        name, type = input
        parameter_string +="\t\t<inputParam> \n"
        parameter_string += "\t\t\t<index>"+ str(index) + "</index>\n"
        parameter_string += "\t\t\t<name>" + name + "</name>\n"
        parameter_string += "\t\t\t<type>" + type + "</type>\n"
        parameter_string +="\t\t\t<namespace>http://www.w3.org/2001/XMLSchema</namespace>\n"
        parameter_string +="\t\t</inputParam>\n"
        index= index + 1
    return parameter_string

def add_net_footer():
    net_str = "\t\t</processControlElements>\n"
    net_str += "\t</decomposition>\n"
    return net_str


# Given a list of inputs generates the XML for the net decomposition
# Needs to be preceded with net_decomposition and add_net_input
def add_net_output(all_outputs):

    index = 0
    parameter_string=""

    for output in all_outputs:
        name, type = output
        parameter_string +="\t\t<outputParam> \n"
        parameter_string += "\t\t\t<index>"+ str(index) + "</index>\n"
        parameter_string += "\t\t\t<name>" + name + "</name>\n"
        parameter_string += "\t\t\t<type>" + type + "</type>\n"
        parameter_string +="\t\t\t<namespace>http://www.w3.org/2001/XMLSchema</namespace>\n"
        parameter_string +="\t\t</outputParam>\n"
        index= index + 1

    return parameter_string

# XML to start a new net decomposition
def net_decomposition(net_name):
    net_str = "\t<decomposition id=\"" +net_name+ "\" isRootNet=\"true\" xsi:type='NetFactsType'>\n"
    net_str += "\t\t<processControlElements>\n"
    return net_str

# Given a list of inputs generates the XML for the net decomposition
# Needs to be preceded with net_decomposition
def add_decomposition_input(all_inputs):
    index = 0
    parameter_string=""

    for name in all_inputs:
        type = "String"
        parameter_string +="\t\t<inputParam> \n"
        parameter_string += "\t\t\t<index>"+ str(index) + "</index>\n"
        parameter_string += "\t\t\t<name>" + name + "</name>\n"
        parameter_string += "\t\t\t<type>" + type + "</type>\n"
        parameter_string +="\t\t\t<namespace>http://www.w3.org/2001/XMLSchema</namespace>\n"
        parameter_string +="\t\t</inputParam>\n"
        index= index + 1

    return parameter_string

# Given a list of inputs generates the XML for the net decomposition
# Needs to be preceded with net_decomposition and add_net_input
def add_decomposition_output(all_outputs):
    index = 0
    parameter_string=""

    for name in all_outputs:
        type = "String"
        parameter_string +="\t\t<outputParam> \n"
        parameter_string += "\t\t\t<index>"+ str(index) + "</index>\n"
        parameter_string += "\t\t\t<name>" + name + "</name>\n"
        parameter_string += "\t\t\t<type>" + type + "</type>\n"
        parameter_string +="\t\t\t<namespace>http://www.w3.org/2001/XMLSchema</namespace>\n"
        parameter_string +="\t\t</outputParam>\n"
        index= index + 1
    return parameter_string

# Generates a set of variables, currently using the same set for network input and output
''' Not in use '''
def create_net():
    '''
        input1 = ("truck1", "string")
        input2 = ("truck2", "string")
        all_inputs = []
        all_inputs.append(input1)
        all_inputs.append(input2)
        return all_inputs
        '''


# Generate XML string given a list of tasks
# Task has the following structure (id, name, next_task, [input1, input2, input3])
def create_tasks(task_names):
    # First task
    task_id, task_name, next_task_id, input  = task_names[0]

    task_string = ""
    task_string += "\t\t\t<inputCondition id=\"InputCondition_1\">\n"
    task_string +=  "\t\t\t<flowsInto>\n"
    task_string += "\t\t\t\t<nextElementRef id='"+ task_id + "' />\n"
    task_string += "\t\t\t</flowsInto>\n"
    task_string +=  "\t\t\t</inputCondition>\n"
    decomposition_string = ""

    # Middle tasks
    index = 0
    while index < len(task_names):
        task_id, task_name, next_task_id, input = task_names[index]
        task_string += "\t\t\t<task id=\"" + task_id + "\">\n"
        task_string += "\t\t\t<name>" + task_name + "</name>\n"
        task_string += "\t\t\t<flowsInto>\n"
        task_string += "\t\t\t\t<nextElementRef id=\"" + next_task_id +"\" />\n"
        task_string += "\t\t\t</flowsInto>\n"
        task_string += "\t\t\t<join code=\"xor\" />\n"
        task_string += "\t\t\t<split code=\"and\" />\n"
        task_string += "\t\t\t <decomposesTo id=\""+ task_id +"\" />\n"
        task_string += "\t\t\t</task>\n"
        decomposition_string += add_task_decomposition(task_id, input, [])
        index += 1

    # End task
    task_string += "\t\t\t<outputCondition id=\"End\" />\n"
    #task_string += "\t\t\t\t<name>End</name>\n"
    #task_string += "\t</outputCondition>\n"
    task_string +=     add_net_footer()
    task_string += decomposition_string
    return task_string


'''
    Task decomposition
    '''
def add_task_decomposition(task_id, all_inputs, all_outputs):
    decomposition_str ="\t<decomposition id=\""+ task_id +"\" xsi:type=\"WebServiceGatewayFactsType\"> \n"
    if (len(all_inputs)>0): decomposition_str += add_decomposition_input(all_inputs)
    if (len(all_outputs)>0): decomposition_str += add_decomposition_output(all_outputs)
    decomposition_str += "\t\t<externalInteraction>manual</externalInteraction>\n"
    decomposition_str += "\t</decomposition>\n"
    return decomposition_str

# Returns canned XML footer
def add_footer():
    footer = """  </specification>
        </specificationSet>"""
    return footer

# Generate XML for a YAML file
def generate_yawl(data, filename):
    clear_file(filename)
    append_to_file(filename, add_header())
    append_to_file(filename, net_decomposition("main_net"))
    #    append_to_file(filename, add_net_input(create_net()))
    #    append_to_file(filename, add_net_output(create_net()))
    append_to_file(filename, create_tasks(data))
    append_to_file(filename, add_footer())


