
"""
Utilities for parsing STRIPS plan (a comma separated list of actions)
into a task list.

Primarily used for generating YAWL workflows from STRIPS plans.

"""

# Generic append to file
def append_to_file(filename, text):
    f = open(filename, 'a')
    f.write(text)
    f.close()


# Used to clear file before writing anything
def clear_file(filename):
    open(filename, 'w').close()


# Generic read from file
def read_from_file(filename):
    text = open(filename, 'r').read()
    return text


# Read solution file as input to YAWL
def parse_plan_for_yawl(filename, data=None):
    if data is None:
        raw_text = read_from_file(filename)
        return parse_plan_for_yawl(filename, raw_text)
    
    task_counter = 0
    raw_tasks = data.split(",")
    # First three items are related to the planner
    raw_tasks = raw_tasks[3:]
    clean_tasks = []
    
    # flat list of tasks
    for task in raw_tasks:
        # add spaces before and after brackets for easier parsing
        tmp = task.replace('(', '( ').replace(')', ' )')
        # split action
        task_parts = tmp.split(" ")
        # drop the brackets
        task_details = task_parts[1:-1]
        # the action name is the first item
        task_name_clean = task_details[0]
        # input variables
        input_variables = task_details[1:]
        clean_tasks.append( (task_name_clean, input_variables) )
    
    task_tuples = []
    # join each task with its "next" task
    while task_counter < len(clean_tasks)-1:
        
        # task_id uses a global counter to make it unique
        task_name, task_objects = clean_tasks[task_counter]
        
        task_id = task_name + str(task_counter)
        task = (task_id, task_name,
                clean_tasks[task_counter+1][0]+str(task_counter+1),
                task_objects)
        task_tuples.append(task)
        task_counter += 1
    
    task_name, task_objects = clean_tasks[-1]
    task = (task_name + str(task_counter), task_name, "End", task_objects)
    task_tuples.append(task)
    return task_tuples

