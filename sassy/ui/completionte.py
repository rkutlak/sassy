# Based on
# rowinggolfer.blogspot.co.uk/2010/08/qtextedit-with-autocompletion-using.html

import logging
logging.getLogger(__name__).addHandler(logging.NullHandler())

def get_log():
    return logging.getLogger(__name__)

    
from PyQt5 import QtWidgets, QtCore, QtGui


class DictionaryCompleter(QtWidgets.QCompleter):
    def __init__(self, parent=None):
        words = []
        try:
            f = open("/usr/share/dict/words","r")
            for word in f:
                words.append(word.strip())
            f.close()
        except IOError:
            print("dictionary not in anticipated location")
        QtWidgets.QCompleter.__init__(self, words, parent)


# mark the end of history - any n < -1
uninitialised = -10

class CompletionTextEdit(QtWidgets.QTextEdit):

    # define a signal that is emited when user presses return (edit finished)
    submit = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(CompletionTextEdit, self).__init__(parent)
        self.setMaximumHeight(90)
        self.completer = None
        self.history = []
        self.history_idx = uninitialised
        self.last_hist_key = None
        self.moveCursor(QtGui.QTextCursor.End)

    def setCompleter(self, completer):
        if self.completer:
            self.completer.disconnect()
        if not completer:
            return

        completer.setWidget(self)
        completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.completer = completer
        self.completer.activated.connect(self.insertCompletion)

    def insertCompletion(self, completion):
        """ User selected an item from the completion list - insert it. """
        tc = self.textCursor()
        # if we are in a middle of a word, replace it
        #   check by looking at the last character (space vs letter)
        if tc.position() == 0: return
        tc.movePosition(QtGui.QTextCursor.Left, QtGui.QTextCursor.KeepAnchor)
        selected_text = tc.selectedText()
        if selected_text != ' ':
            tc.select(QtGui.QTextCursor.WordUnderCursor)
        else:
            tc.movePosition(QtGui.QTextCursor.Right)
        tc.insertText(completion)
        tc.movePosition(QtGui.QTextCursor.EndOfLine)
        self.setTextCursor(tc)

    def textUnderCursor(self):
        tc = self.textCursor()
        tc.select(QtGui.QTextCursor.WordUnderCursor)
        return tc.selectedText()

    def focusInEvent(self, event):
        if self.completer:
            self.completer.setWidget(self);
        QtWidgets.QTextEdit.focusInEvent(self, event)

    def keyPressEvent(self, event):
        if self.completer and self.completer.popup().isVisible():
            if event.key() in (QtCore.Qt.Key_Enter,
                               QtCore.Qt.Key_Return,
                               QtCore.Qt.Key_Escape,
                               QtCore.Qt.Key_Tab,
                               QtCore.Qt.Key_Backtab):
                self.history_idx = uninitialised
                event.ignore()
                return

        # if a user presses enter when not in autocomplete, emit 'submit' signal
        elif self.completer and not self.completer.popup().isVisible():
            if event.key() in (QtCore.Qt.Key_Enter,
                               QtCore.Qt.Key_Return):
                self._history_append(self.toPlainText())
                self.submit.emit()
                event.ignore()
                return

        if len(self.history) > 0:
            if event.key() == QtCore.Qt.Key_Up:
                self._handle_history_back(event.key())
            elif event.key() == QtCore.Qt.Key_Down:
                self._handle_history_forward(event.key());

        ## has ctrl-E been pressed??
        isShortcut = (event.modifiers() == QtCore.Qt.ControlModifier and
                      event.key() == QtCore.Qt.Key_E)
        if (not self.completer or not isShortcut):
            QtWidgets.QTextEdit.keyPressEvent(self, event)

        ## ctrl or shift key on it's own??
        ctrlOrShift = event.modifiers() in (QtCore.Qt.ControlModifier ,
                QtCore.Qt.ShiftModifier)
        if ctrlOrShift and event.text() == '':
            # ctrl or shift key on it's own
            return

#        eow = "~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-=" #end of word

        hasModifier = ((event.modifiers() != QtCore.Qt.NoModifier) and
                        not ctrlOrShift)

        completionPrefix = self.textUnderCursor()

        if (not isShortcut and (hasModifier or
                                event.text() == '')):
            self.completer.popup().hide()
            return

        if (completionPrefix != self.completer.completionPrefix()):
            self.completer.setCompletionPrefix(completionPrefix)
            popup = self.completer.popup()
            popup.setCurrentIndex(
                self.completer.completionModel().index(0,0))

        cr = self.cursorRect()
        cr.setWidth(self.completer.popup().sizeHintForColumn(0)
            + self.completer.popup().verticalScrollBar().sizeHint().width())
        self.completer.complete(cr) ## popup it up!

    def clearHistory(self):
        self.history = []

    def _handle_history_back(self, key):
        print('hist back index: ' + str(self.history_idx))
        # do not show the most recent item twice
        if self.history_idx != uninitialised and self.last_hist_key != key:
            self.history_idx -= 1

        self.last_hist_key = key
        if self.history_idx == uninitialised:
            self.tmp_hist = self.toPlainText()
            self.history_idx = len(self.history) - 1
            self._set_text_from_hist(self.history[self.history_idx])
        elif self.history_idx > 0:
            self.history_idx -= 1
            self._set_text_from_hist(self.history[self.history_idx])
        elif self.history_idx == 0:
            return
        else:
            print('Blast!')

    def _handle_history_forward(self, key):
        print('hist forward index: ' + str(self.history_idx))
        # do not show the most recent item twice
        if self.history_idx != uninitialised and self.last_hist_key != key:
            self.history_idx += 1

        self.last_hist_key = key
        if self.history_idx == uninitialised:
            return
        elif self.history_idx < len(self.history):
            self._set_text_from_hist(self.history[self.history_idx])
            self.history_idx += 1
        elif self.history_idx == len(self.history):
            self._set_text_from_hist(self.tmp_hist)
            self.history_idx = uninitialised
        else:
            print('Blast!')

    def _set_text_from_hist(self, text):
        tc = self.textCursor()
        self.setText(text)
        tc.movePosition(QtGui.QTextCursor.EndOfLine)
        self.setTextCursor(tc)

    def _history_append(self, text):
        self.history_idx = uninitialised
        if text is None or text == '': return
        if len(self.history) > 0:
            if text == self.history[-1]: return
        self.history.append(text)


################################################################################


if __name__ == "__main__":

    app = QtWidgets.QApplication([])
    completer = DictionaryCompleter()
    te = CompletionTextEdit()
    te.setCompleter(completer)
    te.show()
    app.exec_()
