import os
import sys
import random
import logging
from copy import deepcopy

import PyQt5.QtGui as QtGui
import PyQt5.QtCore as QtCore
import PyQt5.QtWidgets as QtWidgets

from controller import Controller
import ui.sassy_main_window as v1
import ui.sassy_main_window_v2 as v2
import utils

from graph import qtgraph

from dstruct import Document, SassyException, Simulation
from nlg_structures import summarise_workflow



MAC = True
try:
    from PyQt5.QtGui import qt_mac_set_native_menubar
except ImportError:
    MAC = False


logging.getLogger(__name__).addHandler(logging.NullHandler())

def get_log():
    return logging.getLogger(__name__)


class SassyMainWindow_V2(QtWidgets.QMainWindow, v2.Ui_SassyMainWindow):
    def __init__(self, parent=None, f=QtCore.Qt.WindowFlags()):
        QtWidgets.QMainWindow.__init__(self, parent, f)
        # set up the widgets
        self.setupUi(self)
        self.flowchart_sa.setBackgroundRole(QtGui.QPalette.Dark)

        # set signals/slots
        self.open_file_btn.clicked.connect(self.force_layout)
        self.open_file_le.returnPressed.connect(self.handle_load)
        self.dialog_line.submit.connect(self.handle_dialog)

        self.filter_line.returnPressed.connect(self.handle_filter)
        self.group_by_line.returnPressed.connect(self.handle_group_by)
        self.filtered_button.clicked.connect(self.handle_filter)
#        self.direct_button.clicked.connect(self.handle_direct)
#        self.indirect_button.clicked.connect(self.handle_indirect)
#        self.required_button.clicked.connect(self.handle_required)
        self.direct_button.clicked.connect(self.handle_filter)
        self.indirect_button.clicked.connect(self.handle_filter)
        self.required_button.clicked.connect(self.handle_filter)
        self.reset_filter_button.clicked.connect(self.handle_reset_filter)
        self.appy_groups_button.clicked.connect(self.handle_group_by)

        self.actionZoom_In.triggered.connect(self.handle_zoom_in)
        self.actionZoom_Out.triggered.connect(self.handle_zoom_out)
        self.actionNormal_Size.triggered.connect(self.handle_normal_size)
        self.actionFit_To_Window.triggered.connect(self.handle_fit_to_window)
        self.actionOpen.triggered.connect(self.handle_open)
        self.actionStart_Executing.triggered.connect(self.start_execution)
        self.actionNext_Instruction.triggered.connect(self.next_instruction)
        self.actionPrint.triggered.connect(self.print_graph)

        self.actionHide_Start.triggered.connect(self.handle_hide_start)
        self.actionHide_Finish.triggered.connect(self.handle_hide_finish)

        # set up autocomplete for path
        completer = QtWidgets.QCompleter(self)
        completer.setModel(QtWidgets.QDirModel(completer))
        self.open_file_le.setCompleter(completer)

        # autocomplete for dialog
        self.commands = ["why", "not", "in", "out", "undec", 
                         "help", "print kb", "print af",
                         "next", "assert", "retract"]

        completer2 = QtWidgets.QCompleter(sorted(self.commands), self);
        completer2.setCaseSensitivity(QtCore.Qt.CaseInsensitive);
        self.dialog_line.setCompleter(completer2);

        # create the controller
        self.ctrl = Controller()
        self.graphWidget = None # placeholder for the graph widget
        self.update_menu()
#        self.default_path = os.sep.join([QtCore.QDir.currentPath(), '..', '..'])
        self.current_path = QtCore.QDir.currentPath()
        file_path = [self.current_path, 'sassy', 'test', 'data', 'Logistics1.sassy']
        self.default_path = '/'.join(file_path)
        if not QtCore.QDir(self.default_path).exists():
            self.default_path = ''
        self.open_file_le.setText(self.default_path)

    def closeEvent(self, event):
        import nlg.realisation as realisation
        get_log().info('calling cleanup()')
#        realisation.default_server.shutdown()

    def force_layout(self):
        if self.graphWidget is not None:
            self.graphWidget.\
            setForceLayout(not self.graphWidget.selfAlign)

    def handle_open(self):
        FD = QtWidgets.QFileDialog
        if utils.MAC:
            dir = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File',
                                                        self.default_path)
            if '' != dir[0]:
                folder = dir[0]
        else:
            dir = FD.getExistingDirectory(self, 'Open Project Folder',
                                      self.default_path,
                                      FD.ShowDirsOnly|FD.DontResolveSymlinks)
            folder = dir

        self.open_file_le.setText(folder)
        self.handle_load()

    def handle_load(self):
        """ Try to load a document from the path given in the text field. """
        folder = self.open_file_le.text()

        # windows can't use a folder as a file - be nice
        if (folder.endswith('domain.ttl') or 
            folder.endswith('workflow.yawl') or 
            folder.endswith('knowledge_base.kb.txt')):
            tmp = folder.rpartition('/')[0]
            if tmp == '':
                tmp = folder.rpartition('\\')[0]
            get_log().debug('Path adjusted to point to folder "%s"' % tmp)
            folder = tmp
            self.open_file_le.setText(tmp)
        # inform the user
        self.dialog_text.setPlainText('')
        self.dialog_text.append('Loading file "%s"' % folder)
        self.filter_line.setText('')
        self.group_by_line.setText('')
        try:
            self.ctrl.load_document(folder)
            self.graphWidget = self.ctrl.get_graph_widget()
            self.flowchart_sa.setWidget(self.graphWidget)
            arguments = self.ctrl.get_arguments()
            cs = [str(a.conclusion) for a in arguments] + self.commands
            completer = QtWidgets.QCompleter(sorted(set(cs)))
            self.dialog_line.setCompleter(completer)
            self.load_plan_summary()
            self.update_ui()
            self.handle_fit_to_window()
        except SassyException as msg:
            # inform the user about failures
            self.dialog_text.append(str(msg))
            self.flowchart_sa.setWidget(None)
            self.graphWidget = None
        self.update_menu()

    def handle_dialog(self):
        sentence = self.dialog_line.toPlainText().strip()
        if sentence == '': return
        self.dialog_line.setText('')
        self.dialog_text.append('USER> ' + sentence)
        if self.ctrl.document is None:
            response = 'SYSTEM> Please load a document first.'
        else:
            # save the planned tasks, in case they change
            self.planned_next = deepcopy(self.ctrl.next_tasks())
            response = self.ctrl.handle_dialog(sentence)
        get_log().info('dialog response: ' + response)
        self.dialog_text.append(response)
        self._reload_instructions()

    def start_execution(self):
        try:
            # can we save the current state of the graphics?
            info = self._save_graph_state()
            folder = self.open_file_le.text()
            self.ctrl.load_document(folder)
            self.graphWidget = self.ctrl.get_graph_widget()
            self.flowchart_sa.setWidget(self.graphWidget)
            self.load_plan_summary()
            self.update_ui()
            if info:
                self._load_graph_state(info)
                
        except SassyException as msg:
            get_log().exception(msg)
            # inform the user about failures
            self.dialog_text.append(str(msg))
            self.graphWidget = None
        self.update_menu()
        self.ctrl.start_simulation()
        self.planned_next = None
        self.graphWidget.setDefaultColours()
        self._reload_instructions()

    def _save_graph_state(self):
        """ Save the state of the graph. """
        info = dict()
        try:
            info['scale'] = self.graphWidget.currentFactor
            for k, v in self.graphWidget.nodes.items():
                val = dict()
                val['x'] = v.pos().x()
                val['y'] = v.pos().y()
                val['colour'] = v.colour
                val['borderColour'] = v.borderColour
                info[k] = val
        except Exception as e:
            get_log().exception(e)
            info = None
        return info
        
    def _load_graph_state(self, info):
        get_log().debug('reloading gtaph state')
        for k, v in info.items():
            if k in self.graphWidget.nodes:
                node = self.graphWidget.nodes[k]
                node.setPos(v['x'], v['y'])
                node.setColour(v['colour'])
                node.setBorderColour(v['borderColour'])
        self.graphWidget.scaleView(info['scale'])

    def next_instruction(self):
        # update colours in the graph
        if self.ctrl.simulation_is_finished():
            self._reload_instructions()
            return
        green_tasks, _ = self.ctrl.current_tasks()
        for t in green_tasks:
            self.graphWidget.setColour(t, 'lime')
        self.ctrl.simulation_step_forward()
        self._reload_instructions()

    def _reload_instructions(self):
        """ Update the colours in the graph and the corresponding text + UI. """
        # update UI buttons
        finished = self.ctrl.simulation_is_finished()
        self.actionNext_Instruction.setEnabled(not finished)
        if finished: return
        queued_tasks = self.ctrl.simulation_get_tasks_in_queue()
        all_green =[]
        all_red = []
        if queued_tasks is not None:
            all_green, all_red = queued_tasks
        # update colours in the graph
        current_tasks = self.ctrl.current_tasks()
        if current_tasks:
            green_tasks, red_tasks = current_tasks
            for t in green_tasks:
                self.graphWidget.setColour(t, 'green')
            for t in red_tasks:
                if t not in all_green:
                    self.graphWidget.setColour(t, 'red')
        if self.planned_next:
            tasks, foo = self.planned_next
            for t in tasks:
                self.graphWidget.setColour(t, '#6E6E6E')
            self.planned_next = None # TODO: should this not be reset in Next?
        # pre-colour the next tasks
        next_tasks = self.ctrl.next_tasks()
        if next_tasks:
            green_tasks, red_tasks = next_tasks
            for t in green_tasks:
                self.graphWidget.setColour(t, '#81F7BE')
            for t in red_tasks:
                if t not in all_green:
                    self.graphWidget.setColour(t, '#F5BCA9')

        self.workflow_text.setPlainText('Current task: %s\n' %
            self.ctrl.current_tasks_as_text())
        self.workflow_text.append('Next task: %s\n' %
            self.ctrl.next_tasks_as_text())

# UI related methods

    def update_menu(self):
        doc_loaded = self.ctrl.document is not None
        self.actionFit_To_Window.setEnabled(doc_loaded)
        self.actionZoom_In.setEnabled(doc_loaded)
        self.actionZoom_Out.setEnabled(doc_loaded)
        self.actionNormal_Size.setEnabled(doc_loaded)
        self.actionStart_Executing.setEnabled(doc_loaded)
        self.actionNext_Instruction.setEnabled(doc_loaded)
        self.actionPrint.setEnabled(doc_loaded)

    def update_ui(self):
        self.handle_normal_size()

    def handle_zoom_in(self):
        factor = 1.2
        self.graphWidget.scaleView(factor)

    def handle_zoom_out(self):
        factor = 1/1.2
        self.graphWidget.scaleView(factor)

    def handle_normal_size(self):
        self.graphWidget.scaleView(1.0)

    def handle_fit_to_window(self):
        self.graphWidget.fitToWidith(self.flowchart_sa.width()-20)

    def load_plan_summary(self):
        text = 'No summary available.'
        if self.ctrl.document is not None:
            nlg_doc = summarise_workflow(self.ctrl.document.workflow)
            text = self.ctrl.document_to_text(nlg_doc)
            text += '\n\n'
            # apply filter as well
            filter_items = self._get_filter_items()
            text += self.ctrl.workflow_as_text(self.ctrl.document, filter_items)
        self.workflow_text.setPlainText(text)

    def handle_filter(self):
        for k, n in self.graphWidget.nodes.items():
            n.setBorderColour('black')
            self.graphWidget.showNode(k)
        self.handle_filtered()
        self.handle_hide_start()
        self.handle_hide_finish()
        self.flowchart_sa.update()
        self.load_plan_summary()

    def handle_reset_filter(self):
        print('Resetting filter.')
        self.filter_line.setText('')
        for k, n in self.graphWidget.nodes.items():
            n.setBorderColour('black')
            self.graphWidget.showNode(k)
        self.filtered_button.setChecked(False)
        self.direct_button.setChecked(True)
        self.indirect_button.setChecked(True)
        self.required_button.setChecked(True)
        self.actionHide_Start.setChecked(False)
        self.actionHide_Finish.setChecked(False)

    def handle_group_by(self):
        text = self.group_by_line.text().strip()
        if len(text) == 0:
            things = []
        else:
            things = [x.strip() for x in text.split(',')]
        groups = dict()
        for i, t in enumerate(things):
            items = self.ctrl.filter([t])
            groups[i] = items
        self.graphWidget.autoLayout(groups)

    def _get_filter_items(self):
        """ Return the list of objects and types to filtery by. """
        text = self.filter_line.text().strip()
        if len(text) == 0:
            return []
        things = [x.strip() for x in text.split(',')]
        return things

    def handle_direct(self):
        """ Show or hide tasks directly related to the filter object. """
        things = self._get_filter_items()
        selected = self.ctrl.filter(things)
        if self.direct_button.isChecked():
            for t in selected:
                self.graphWidget.setBorderColour(t, 'yellow')
            return set(selected)
        else:
            for t in selected:
                self.graphWidget.setBorderColour(t, 'black')
            return set()

    def handle_indirect(self):
        """ Show or hide tasks indirectly related to the filter object. """
        things = self._get_filter_items()
        tasks = self.ctrl.filter(things)
        tasks2 = self.ctrl.filter_inclusive(things)
        selected = (tasks2 - tasks)
        if self.indirect_button.isChecked():
            for t in selected:
                self.graphWidget.setBorderColour(t, 'yellow')
            return set(selected)
        else:
            for t in selected:
                self.graphWidget.setBorderColour(t, 'black')
            return set()

    def handle_required(self):
        """ Show or hide tasks that are required by AND joins in a plan."""
        things = self._get_filter_items()
        tasks = self.ctrl.filter_inclusive(things)
        related = self.ctrl.get_related_tasks(tasks)
        selected = related - tasks
        if (self.required_button.isChecked()):
            for t in selected:
                self.graphWidget.setBorderColour(t, 'blue')
            return set(selected)
        else:
            for t in selected:
                self.graphWidget.setBorderColour(t, 'black')
            return set()

    def handle_filtered(self):
        """ Show or hide tasks that were filtered out. """
        things = self._get_filter_items()
        if len(things) == 0: return
        all = self.ctrl.get_all_tasks()
        direct = self.handle_direct()
        indirect = self.handle_indirect()
        required = self.handle_required()
        
        to_hide = all - direct - indirect - required
                
        if self.filtered_button.isChecked():
            for t in to_hide:
                self.graphWidget.hideNode(t)
        else:
            for k, n in self.graphWidget.nodes.items():
                self.graphWidget.showNode(k)

        self.graphWidget.update()
        self.flowchart_sa.update()

    def print_graph(self):
        dir = QtWidgets.QFileDialog.getSaveFileName(self, 'Save Workflow Graph',
                                                    QtCore.QDir.currentPath())
        if '' != dir[0]:
            self.graphWidget.savePDF(dir[0])
            self.dialog_text.append('Graph saved in "%s"' % dir[0])

    def handle_hide_start(self):
        action = self.actionHide_Start
        start = list(self.ctrl.get_start())
        if len(start) > 0:
            if action.isChecked():
                self.graphWidget.hideNode(start[0])
            else:
                self.graphWidget.showNode(start[0])

    def handle_hide_finish(self):
        action = self.actionHide_Finish
        finish = list(self.ctrl.get_end())
        if len(finish) > 0:
            if action.isChecked():
                self.graphWidget.hideNode(finish[0])
            else:
                self.graphWidget.showNode(finish[0])

    def resizeEvent(self, event):
        super().resizeEvent(event)
        if self.actionFit_To_Window.isChecked():
            self.graphWidget.fitToWidith(self.flowchart_sa.width() - 20)


#############################################################################
##
## Copyright (C) 2013 Roman Kutlak, University of Aberdeen.
## All rights reserved.
##
## This file is part of SAsSy Demonstrator program.
##
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of University of Aberdeen nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################
