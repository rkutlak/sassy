import os
import logging

import formats.yawl as yawl
from nlg.ontology import Ontology
from nlg.nlg import Nlg
from nlg_structures import TaskMsgSpec
from argumentation.kb import KnowledgeBase
from argumentation.dialog import Dialog
import argumentation.players as players
import argumentation.common as common
from graph.qtgraph import Graph
from graph.qtgraph import GraphWidget


# add default logger
logging.getLogger('sasssy').addHandler(logging.NullHandler())

def get_log():
    return logging.getLogger('sasssy')


class SassyException(Exception):
    """ A geeral exception class. """
    pass

class InvalidPathException(SassyException):
    """ Exception indicating that a path does not exist. """
    pass

class InvalidFormatException(SassyException):
    """ Exception indicating that data are not in an expected format. """
    pass


class World:
    """ The class World allows adding and removing knowledge based 
    on preconditions. It is a simple hack that allows the simulation 
    to act as if the world changed (sensors, time, etc. )
    
    The class has two dictionarries with preconditions as cues and a list
    of strings in the format of rules. The dictionary 'assertions' is used
    to assert new facts and the dictionary 'retractions' is used to retract
    facts from the knowledge base.
    """
    def __init__(self, assertions, retractions):
        self.assertions = assertions if assertions is not None else dict()
        self.retractions = retractions if retractions is not None else dict()


class Simulation:
    """ Class used for executing a workflow. It is responsible for maintaining
    the state of the execution, which entails the current task(s) and the tasks
    that need to be done next. There can be more than one current task,
    in which case the method current_tasks() returns a list of tasks 
    that should be done next.
    
    """
    def __init__(self, document, world=None):
        if document is None:
            raise SassyException("document required to run simmulation")
        self.document = document
        self.world = world
        self.task_queue = []
        self.done_tasks = set()
        self.excluded_tasks = set()

    def start(self):
        self.time = 0
        self.task_queue = []
        self.task_queue.append( ([self.document.workflow.starting_task()], []) )
        self.done_tasks = set()
        self._add_next_to_queue()

    def is_finished(self):
        return (len(self.task_queue) == 0)

    def current_tasks(self):
        """ Return the current tasks to execute from the queue. 
        The function returns two lists as a tuple, where the first list are
        tasks that are possible to execute and the second list contains tasks
        that are not possible to execute.

        """
        # the current task is the last in the queue
        if self.is_finished(): return None
        return self.task_queue[-1]

    def next_tasks(self):
        """ Return the tasks that follow the current ones.
        The function returns two lists as a tuple, where the first list are
        tasks that are possible to execute and the second list contains tasks
        that are not possible to execute.
        
        """
        if (len(self.task_queue) > 1):
            return self.task_queue[-2]
        return None

    def step_forward(self):
        """ Assert that the current tasks were done and move to next task. 
        We assume that for mutually exclusive choices, the KB decides which 
        ONE of the choices should be taken. In particular, if more than one
        mut. ex. choice is possible, the behaviour is undefined. 

        """
        # disable rule recalculating
        self.document.dialog.kb.batch = True
        self._update_time()
        # next save the current tasks as the result and update the KB
        green, red = self.current_tasks()
        for t in green:
            get_log().info(self.document.dialog.parse('assert done_%s' % t.id))
            get_log().info(self.document.dialog.parse('retract doing_%s' % t.id))
            self.done_tasks.add(t)
#            try:
#                self.document.dialog.add(rule(' done_%s' % t.id))
#                self.document.dialog.delete(rule(' doing_%s' % t.id))
#                self.document.dialog.recalculate()
#                self.done_tasks.add(t)
#            except Exception as e:
#                get_log().exception(e)

        result = self.task_queue.pop()
        self._add_next_to_queue() # add param?
        if self.is_finished(): return result

        # add into the KB the current tasks as doing_Task
        poss, imposs = self.current_tasks()
        for t in self.done_tasks:
            if t in poss:
                poss.remove(t)
        if poss == []:
            self.step_forward()
        for x in poss:
            get_log().info(self.document.dialog.parse('assert doing_%s' % x.id))

        # disable rule recalculating
        self.document.dialog.kb.batch = False
        self.document.dialog.kb.recalculate()
        
        self._add_next_to_queue()
        
        return result

    def set_next_task(self, task):
        """ Force the simulation to add the given task to the front 
        of the queue. 
        
        """
        # TODO: add a method to filter the queue and use that as well as excluded_tasks
        if self.is_finished(): return None
        current, _ = self.current_tasks()
        if current[0].split_type == 'and':
            tmp = self.task_queue.pop()
            self._filter_queue([task], [])
            self.task_queue.append( ([task], []) )
            self.task_queue.append(tmp)
        if current[0].split_type == 'xor':
            tmp = self.task_queue.pop()
            excluded = set(current[0].flows_into) - {task}
            self._filter_queue(list(excluded), [])
            self.task_queue.append( ([task], [] ) )
            self.task_queue.append(tmp)
#        self.document.dialog.parse('assert doing_%s' % task)
#        self._add_next_to_queue()
        return True

    def recalculate(self):
        """ Store the current node and re-calculate what tasks to do next. """
        on_queue = self.task_queue[:]
        self.task_queue = []
        for green, red in on_queue:
            possible = set()
            impossible = set()
            for t in green + red:
                if self.document.dialog.is_accepted_conclusion(t.id):
                    possible.add(t)
                    get_log().info('%s added to possible' % str(t.id))
                # TODO: this seems like a hack. why is this not accepted concl.?
                elif (t.id == 'inputCondition' or t.id.lower() == 'start'):
                    possible.add(t)
                    get_log().info('%s added to possible' % str(t.id))
                else:
                    impossible.add(t)
                    get_log().info('%s added to impossible' % str(t.id))
            self.task_queue.append( (list(possible), list(impossible)) )
        self._add_next_to_queue()

    def get_tasks_on_queue(self):
        """ Return tasks that are currently on the task_queue. """
        if self.is_finished(): return None
        green_tasks = []
        red_tasks = []
        for g, r in self.task_queue[:-1]:
            green_tasks.extend(g)
            red_tasks.extend(r)
        green_tasks = list(set(green_tasks))
        red_tasks = list(set(red_tasks))
        return (green_tasks, red_tasks)

    def _filter_queue(self, green, red):
        """ Remove the given tasks from the queue. """
        get_log().info('** old queue: %s' % str([x[0][0].id for x in self.task_queue]))
        get_log().info('** filtering queue: %s' % str([x.id for x in green]))
        for g, r in self.task_queue:
            for t in green:
                if t in g:
                    g.remove(t)
            for t in red:
                if t in r:
                    r.remove(t)
        self.task_queue = [ (g, r) for g, r in self.task_queue if g != [] ]
        get_log().info('** new queue: %s' % str([x[0][0].id for x in self.task_queue]))

    def _is_in_queue(self, task):
        """ Return True if task is in the queue (as green). """
        for g, r in self.task_queue:
            if task in g: return True
        return False

    def _add_next_to_queue(self):
        """ Calculate the next set of tasks and add them to the queue. """
        get_log().info('\n\n***')
        # first update time -

        # next check that we can do something
        if self.is_finished(): return []
        queued = set()
        for tmp in self.task_queue:
            queued.update(tmp[0])
        get_log().info('queued: %s' % str([str(x) for x in queued]))
        # next save the current tasks as the result and update the KB
        result = self.task_queue[-1]
        green, red = result # split the pair into done and impossible tasks
        # update the KB with data from the world
#        self._update_world()
        # now calculate what tasks should be next
        possible = set()
        impossible = set()
        # although green is a list, more than one element are present only
        # if they all flow into the same node, so we can only use the first elt
        t = green[0]
#        get_log().debug('Potential tasks:\n' + str(t.flows_into))
#        get_log().debug('KB:\n' + str(self.document.dialog.kb))
#        get_log().debug('AAF:\n' + str(self.document.dialog.aaf))
        for potential in t.flows_into:
            if self.document.dialog.is_accepted_conclusion(potential.id):
                possible.add(potential)
                get_log().info('%s is accepted conclusion.' %
                               str(potential.id))
            else:
                impossible.add(potential)
                get_log().info('%s is NOT accepted conclusion.' % 
                               str(potential.id))
#        # remove tasks, that were excluded by other choices
#        if t.split_type == 'xor':
#            following = set(t.flows_into)
#            chosen = following & queued
#            possible -= following - chosen
#            print('following: %s' % [t.id for t in following])
#            print('queued: %s' % [t.id for t in queued])
#            print('chosen: %s' % [t.id for t in chosen])
        # the next task must be not only justified, but all preceding taks
        # leading to this one must be done or currently being done
        invalid_preconditions = set()
        for t in possible:
            if t.join_type == 'and':
                # for AND all tasks must be done
                preconditions = set()
                for k, vals in self.document.workflow.task_graph.items():
                    if t in vals:
                        preconditions.add(k)
                get_log().info('preconditions for %s: %s' %
                        (str(t), str([str(x) for x in preconditions])))
                if (preconditions.intersection(self.done_tasks | queued)
                    != preconditions):
                    invalid_preconditions.add(t)
                    get_log().info('\tfulfilled precondition: %s'
                    %str(preconditions.intersection(self.done_tasks | queued)))
        get_log().info('missing preconditions for: %s' %
                        [x.id for x in invalid_preconditions])
        possible -= invalid_preconditions

        get_log().info('Possible: %s: ' % str([x.id for x in possible]))
        get_log().info('Impossible: %s: ' % str([x.id for x in impossible]))
        if len(possible) == 0: return []
        dests = dict()
        # if no tasks are possible from the current one?
        for p in possible:
            if len(p.flows_into) == 1:
                if p.flows_into[0] not in dests:
                    dests[p.flows_into[0]] = set()
                dests[p.flows_into[0]].add(p)
        # remove duplicates and mark the ones that can go together
        used = set()
        for d, srcs in dests.items():
            used.update(srcs)
        possible -= used # remove the tasks added already
        possible -= self.done_tasks # remove tasks that were already executed
        impossible -= self.done_tasks
        # also remove tasks already on the queue
        possible -= queued
        impossible -= queued

        to_add = []
        for p in possible:
            to_add.append( [p] )
        for d, srcs in dests.items():
            to_add.append(list(srcs))
        to_add = list(sorted(to_add, key=lambda x: len(x)))
        get_log().info('to add:')
        for tmp in to_add:
            printable = list(map(lambda x: str(x), tmp))
            get_log().info('\t%s' % str(printable))

        if len(to_add) == 0: return result
        task = (to_add[0], list(impossible))
        if task not in self.task_queue:
            self.task_queue.insert(0, task)
        for x in to_add[1:]:
            task = (x, [])
            if task not in self.task_queue:
                self.task_queue.insert( 0, task )

        get_log().info('Queue: ')
        for g, r in self.task_queue:
            gr = list(map(lambda x: str(x), g))
            re = list(map(lambda x: str(x), r))
            get_log().info('\t%s  ::  %s' % (str(gr), str(re)))
        # return the added ones
        return result

    def _update_world(self):
        """ Update the KB with data from the world. """
        # TODO:
        #     reduce the complexity by avoiding recalculation in the inner loops
        if self.world is None: return
        # as the KB can grow with new assertinos,
        # this has to be called recursively
        old_size = -1
        while (old_size != len(self.dialog.kb)):
            old_size = len(self.dialog.kb)
            if self.world.assertions is not None:
                for k, v in self.world.assertions.items():
                    if self.document.dialog.is_accepted_conclusion(k):
                        for r in v:
                            self.document.dialog.parse('assert %s' % v)
                        # once the info was added, remove it from the world
                        del self.world.assertions[k]
            if self.world.retractions is not None:
                for k, v in self.world.retractions.items():
                    if self.document.dialog.is_accepted_conclusion(k):
                        for r in v:
                            self.document.dialog.parse('retract %s' % v)
                        # once the info was added, remove it from the world
                        del self.world.retractions[k]

    def _update_time(self):
        """ Update the time by adding the rule into the KB. """
        # TODO: manipulate the AF, not the dialog
        #   maintain current_t as well as timepoints t_1, t_2, t_3, ...
        self.document.dialog.parse('retract --> current_t_%d' % self.time)
        self.document.dialog.parse('assert --> t_%d' % self.time)
        self.time += 1
        self.document.dialog.parse('assert --> current_t_%d' % self.time)
#        get_log().debug(self.document.dialog.delete(
#            rule('--> current_t_%d' % self.time)))
#        get_log().debug(self.document.dialog.add(
#            rule(' --> t_%d' % self.time)))
#        self.time += 1
#        get_log().debug(self.document.dialog.add(
#            rule('--> current_t_%d' % self.time)))
#        self.document.dialog.recalculate()


class Document:
    """ The document class represents a Sassy document. It is a collection
    of information needed for the operation of the application. When a user
    selects a folder to open, the program creates a Document instance with 
    the selected YAWL workflow, its graphical representation and an ontology
    corresponding to the domain, from which natural language is generated.
    
    """
    def __init__(self, path='', name='',
                 workflow=None, onto=None, dialog=None):
        self.path = path
        self.name = name
        self.workflow = workflow
        self.ontology = onto
        self.dialog = dialog

    def __repr__(self):
        result = '(Document %s\n\tname : %s)' % (self.path, self.name)
        return result

    def __str__(self):
        result = self.__repr__()
        return result

    @classmethod
    def from_dir(Cls, path):
        if not os.path.isdir(path):
            msg = 'Folder "%s" does not exist' % path
            raise InvalidPathException(msg)

        tmp = path.split(os.path.sep)
        name = tmp[-1]

        workflow_path = os.path.join(path, 'workflow.yawl')
        if not os.path.exists(workflow_path):
            msg = 'Folder "%s" does not contain "workflow.yawl" file' % path
            raise InvalidPathException(msg)

        workflow = Workflow.from_yawl(workflow_path)

        ontology_path = os.path.join(path, 'domain.ttl')
        onto = Ontology(ontology_path)
        workflow.ontology = onto
        kb_path = os.path.join(path, 'knowledge_base.kb.txt')
        dialog = Dialog(kb_path)

        doc = Cls(path, name, workflow, onto=onto, dialog=dialog)
        return doc


class Workflow:
    """ This class represents a workflow in YAWL.
        It is composed of individual tasks + some metadata.
        
    """
    def __init__(self, id='', ts=None, name='',
                 descr='', ver='', creator='', domain='', ontology=None):
        """ Create a Workflow instance with a TaskGraph. """
        # dict of tasks and corresponding keys e.g., {t1 : Task('t1', ...)}
        self._tasks = dict() if ts is None else ts
        self._construct_task_graph(ts)
        self.id = id
        self.name = name
        self.description = descr
        self.version = ver
        self.creator = creator
        self.domain = domain
        self.ontology = ontology

    @classmethod
    def from_yawl(cls, file):
        """ Read a YAWL file and return a Workflow instance. """
        data = yawl.parse_yawl_file(file)
        tasks = dict()
        for id, t in data.tasks.items():
            new_task = Task(t.id, t.name)
            new_task.split_type = t.split
            new_task.join_type = t.join
            new_task.flows_into = t.flows_into
            did = t.decomposition_id
            if did is not None:
                decomp = data.decs[did]
                params = list()
                for param in decomp.input_params:
                    new_task.input_params.append(param.name)
                for param in decomp.output_params:
                    new_task.output_params.append(param.name)

            new_task.is_input_condition = t.is_input_condition
            new_task.is_condition = t.is_condition
            new_task.is_output_condition = t.is_output_condition

            tasks[id] = new_task
        # map task IDs to actual tasks
        for id , task in tasks.items():
            tasks[id].flows_into = [tasks[x] for x in task.flows_into]

        workflow = cls(id=data.identifier,
                       ts=tasks,
                       name=data.name,
                       descr=data.description,
                       ver=data.version,
                       creator=data.creator,
                       domain=data.domain)
        return workflow

    def _construct_task_graph(self, ts, check=True):
        """ Construct qtgraph.Graph representing the tasks. """
        graph = dict()
        reversed = dict()
        root = None
        top = None
        if ts is not None:
            for k, v in ts.items():
                graph[v] = v.flows_into
                # the reversed graph goes from flows_into to v
                for i in v.flows_into:
                    if i not in reversed: reversed[i] = []
                    reversed[i].append(v)
                if v not in reversed: reversed[v] = []
                if v.is_input_condition:  root = v
                elif v.is_output_condition: top = v
        if check and (root is None or top is None):
            message = 'Workflow is missing Input or Output Condition'
            raise SassyException(message)
        self.task_graph = Graph(graph)
        self.task_graph.root = root
        self.task_graph.top = top
        self.reversed_task_graph = Graph(reversed)
        self.reversed_task_graph.root = top
        self.reversed_task_graph.top = root

    def __repr__(self):
        return self.name

    def __str__(self):
        return 'Workflow %s' % self.__repr__()

    def starting_task(self):
        """ Return the workflow's first task. This method assumes that 
        there is at leas one task in the workflow.

        """
        if self.task_graph.root is not None:
            return self.task_graph.root
        return self._tasks[self.task_graph.nodes()[0]]

    def tasks(self):
        """ Return a generator that goes through the workflow tasks and returns
        them one by one. It is assumed that all tasks are connected.
        
        """
        start = self.starting_task()
        for t in self.task_graph.breadth_first_nodes(start):
            yield self._tasks[t.id]

    def task(self, key):
        """ Return task corresponding to the given key or raise KeyError. """
        return self._tasks[key]

    def tasks_with_object(self, o):
        """ Search the workflow and return tasks that contain given object. """
        result = set()
        for k, task in self._tasks.items():
            if o in task.input_params: result.add(task)
        return result

    def tasks_with_object_inclusive(self, o):
        """ Search the workflow and return tasks that contain given object. """
        result = set()
        for k, task in self._tasks.items():
            if o in task.input_params: result.add(task)
        tasks = set(result)
        # now that we have tasks that directly manipulate the object,
        # add tasks that are on the path between the manipulations
        for t1 in list(result):
            paths = []
            for t2 in list(result):
                if t2 == t1: continue
                ps = self.task_graph.find_all_paths(t1, t2)
                for p in ps: paths.append(p)
            for path in paths:
                for t in path: result.add(t)
        return result

#        for t1 in list(result):
#            paths = []
#            for t2 in list(result):
#                if t2 == t1: continue
#                path = self.task_graph.find_shortest_path(t1, t2)
#                if path: paths.append(path)
#            if len(paths) > 0:
#                tmp = sorted(paths, key=lambda x: len(x))
#                path = tmp[0] # the shortest path
#                for t in path: result.add(t)

    def tasks_with_objects(self, os):
        """ Search the workflow and return tasks that contain given object. """
        result = set()
        for o in os:
            result |= self.tasks_with_object(o)
        return result

    def tasks_with_objects_inclusive(self, os):
        """ Search the workflow and return tasks that contain given object. """
        result = set()
        for o in os:
            result |= self.tasks_with_object_inclusive(o)
        return result

    def tasks_with_type(self, t):
        """ Search the workflow and return rasks that contain an object of 
        given type.
        
        """
        result = set()
        if not self.ontology: return result
        objects = self.ontology.entities_of_type(t)
#        print('objects of type "%s": %s' % (t, str(objects)))
        # strip the namespace
        objects = [o.split(':')[1] for o in objects]
#        print('objects of type "%s": %s' % (t, str(objects)))
        for o in objects:
            result |= self.tasks_with_object(o)
        return result

    def tasks_with_type_inclusive(self, t):
        """ Search the workflow and return rasks that contain an object of 
        given type.
        
        """
        result = set()
        if not self.ontology: return result
        objects = self.ontology.entities_of_type(t)
#        print('objects of type "%s" (incl): %s' % (t, str(objects)))
        # strip the namespace
        objects = [o.split(':')[1] for o in objects]
#        print('objects of type "%s" (incl): %s' % (t, str(objects)))
        for o in objects:
            result |= self.tasks_with_object_inclusive(o)
        return result

    def tasks_with_types(self, ts):
        """ Search the workflow and return rasks that contain an object of 
        given type.
        
        """
        result = set()
        for t in ts:
            result |= self.tasks_with_type(t)
        return result

    def tasks_with_types_inclusive(self, ts):
        """ Search the workflow and return rasks that contain an object of 
        given type.
        
        """
        result = set()
        for t in ts:
            result |= self.tasks_with_type_inclusive(t)
        return result

    def prerequisites(self, task):
#        """ If a task has AND join, return all tasks that lead into this one."""
        """ Return all tasks that lead into this one."""
        result = set()
#        if task.join_type is not None and task.join_type.lower() == 'and':
        result |= set(self.reversed_task_graph[task])
        return result

    def start_and_end(self):
        """ Return the start and end node of a workflow. """
        return {self.task_graph.root, self.task_graph.top}

    def start(self):
        """ Return the start node of a workflow. """
        return {self.task_graph.root}

    def end(self):
        """ Return the end node of a workflow. """
        return {self.task_graph.top}


class Task:
    """ Task contains information about individual tasks in a workflow. """
    def __init__(self, id='', name=None):
        """ Task has the following fields:
            id - string
            name - string
            input_params - list of input parameter IDs
            output_params - list of output parameter IDs
            flows_into - list of task ids
            split_type - SplitType: type of fork (AND, OR, XOR)
            join_type - JoinType: type of join (AND, OR, XOR)

        """
        self.id = id
        # if id is set and name is not, use id
        if name is None or name == '':
            self.name = id
        else:
            self.name = name
        self.input_params = list()
        self.output_params = list()
        self.flows_into = list()
        self.split_type = None
        self.join_type = None
        # yawl has conditions
        #   a workflow should have 1 input condition
        self.is_input_condition = False
        #   a workflow should have 1 output condition
        self.is_output_condition = False
        #   no idea what these are
        self.is_condition = False

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self.id == other.id and
                self.name == other.name and
                self.input_params == other.input_params and
                self.output_params == other.output_params and
                self.split_type == other.split_type and
                self.join_type == other.join_type and
                self.is_condition == other.is_condition and
                self.is_input_condition == other.is_input_condition and
                self.is_output_condition == other.is_output_condition)

    def __hash__(self):
        return (hash(self.id) ^ hash(self.name) ^
                hash(tuple(self.input_params)) ^
                hash(tuple(self.output_params)))

    def __str__(self):
        result = self.id
        if self.name is not None and self.name != '': result = self.name
        return result

    def __repr__(self):
        details = ('id: %s (name: %s, params: %s)' %
                 (self.id, self.name, str(self.input_params)))
        cond = (' (condition)' if self.is_condition else (
                    ' (input condition)' if self.is_input_condition else (
                        ' (output condition)' if self.is_output_condition else (
                            ''))))

        result = '(Task %s%s)' % (details, cond)
        return result


# unused at the moment...
def default_node_colour_fn(id, workflow):
    node = None
    if id in workflow.task_graph:
        node = workflow.task(id)
    if node is None: return 'blue'
    if node.is_input_condition: return 'green'
    if node.is_output_condition: return 'red'
    if node.is_condition: return 'orange'
    return 'blue'


def create_graph_widget_from(workflow,
                             node_colour_fn=default_node_colour_fn,
                             edge_colour_fn=None):
    """Create a graph of the workflow.
    
    workflow (dstruct.Workflow): workflow to graph
    node_colour_fn a callable object that can determine a node colour
    edge_colour_fn a callable object that can determine an edge colour
    
    The colour functions will be called 'c = node_colour_fn(node, workflow)'
    and should return a string that Qt can use as a colour 
    (e.g., '#RRGGBB' or 'blue').

    """
    if not workflow:
        return GraphWidget("", None)
    # create a function for creating labels
    fn = lambda x: labels(workflow.ontology, x)

    graph = GraphWidget(workflow.task_graph,
                        workflow.description,
                        fn)

    def silent_lower(x):
        if x is not None: return x.lower()
        return None

    tg = workflow.task_graph
    rtg = workflow.reversed_task_graph

    for t in tg.nodes:
        if len(tg[t]) > 1:
            n = graph.getNode(t)
            if silent_lower(t.split_type) == 'xor':
                n.addDecorator('right_triangle', 'right')
            elif silent_lower(t.split_type) == 'and':
                n.addDecorator('left_triangle', 'right')
            elif silent_lower(t.split_type) == 'or':
                n.addDecorator('diamond', 'right')
            else:
                get_log().warning('bad split type for task %s' % repr(t))

    for t in rtg.nodes:
        if len(rtg[t]) > 1:
            n = graph.getNode(t)
            if silent_lower(t.join_type) == 'xor':
                n.addDecorator('left_triangle', 'left')
            elif silent_lower(t.join_type) == 'and':
                n.addDecorator('right_triangle', 'left')
            elif silent_lower(t.join_type) == 'or':
                n.addDecorator('diamond', 'left')
            else:
                get_log().warning('bad join type for task %s' % repr(t))

    return graph


def labels(onto, task):
    """ Convert the task to a string and return it. """
    msg_spec = TaskMsgSpec(task)
    nlg_engine = Nlg()
    text = nlg_engine.process_nlg_doc(msg_spec, onto)
    return text


# PDDL stuff


def count_goals(gd):
    """ Take given goal description (logic.Expr) and count how many possible
    goals are in given expression. For example, if the gd is (A and (B or C))
    there are two possible goals: (A and B) and (A and C).
    """
    count = count_goals_hlpr(gd)
    # expression without ORs counts as 1 goal
    return (count or 1)


def count_goals_hlpr(gd):
    """ Helper that does the real count. """
    counts = map(count_goals_hlpr, gd.args)
    tmp = []
    if gd.op.upper() == '|':
        for c in counts:
            tmp.append(c or 1) # c if c is not 0 else 1
    else:
        tmp = list(counts)
    return sum(tmp)





