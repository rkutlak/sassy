import os
import sys
import sip
import utils

# force cx_Freeze include plugins
import rdflib.plugins.memory
import rdflib.plugins.parsers
import rdflib.plugins.serializers
import rdflib.plugins.parsers.notation3


sys.path.append('sassyargumentation')
sys.path.append('sassygraph')
sys.path.append('sassynlg')


from PyQt5.QtWidgets import QApplication
from ui.gui import SassyMainWindow_V2

def run():
    try:
        print('Setting up logging...')
        utils.try_setup_logging()
    except Exception as e:
        print('Exception while setting up logging:' + str(e))
    app = QApplication(sys.argv)
    window = SassyMainWindow_V2()
    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    run()

