import os
import sys
import random
import logging

from copy import deepcopy

from nlg.nlg import Nlg
from graph import qtgraph
from graph.qtgraph import GraphWidget

from dstruct import Document, SassyException, Simulation
from nlg_structures import summarise_workflow
from nlg_structures import task_to_msg_spec
from nlg_structures import tasks_to_msg_spec
from nlg_structures import create_literal_msg
from dstruct import create_graph_widget_from
from nlg.reg import Context
from argumentation.kb import StrictRule, DefeasibleRule

logging.getLogger(__name__).addHandler(logging.NullHandler())

def get_log():
    return logging.getLogger(__name__)


class Controller:
    def __init__(self):
        self.nlg = Nlg()
        self.document = None
        self.graphWidget = None
        self.context = None
        self.simulation = None

    def load_document(self, folder):
        """ Load a document from the given folder. """
        try:
            self.document = Document.from_dir(folder)
            self.graphWidget = create_graph_widget_from(self.document.workflow)
            self.graphWidget.autoLayout()
            self.simulation = Simulation(self.document)
            self.context = Context(self.document.ontology)
            self.dialog = self.simulation.document.dialog
            return self.document
        except SassyException as msg:
            self.document = None
            self.graphWidget = None
            self.context = None
            self.simulation = None
            raise

    def get_graph_widget(self):
        """ Return the widget representing the workflow. """
        if self.graphWidget is None: return None
        return self.graphWidget

# simulation related methods

    def start_simulation(self):
        """ Start the simulation. """
        if self.simulation: self.simulation.start()

    def current_tasks(self):
        """ Return the current tasks. """
        if self.simulation: return self.simulation.current_tasks()
        return None

    def next_tasks(self):
        """ Return the next tasks. """
        if self.simulation: return self.simulation.next_tasks()
        return None

    def simulation_step_forward(self):
        """ Move the simulation by a step. """
        if self.simulation: return self.simulation.step_forward()
        return None

    def simulation_is_finished(self):
        """ Return true if simulation is finished. """
        if self.simulation: return self.simulation.is_finished()
        return True

    def simulation_get_tasks_in_queue(self):
        """ Return all tasks currently on the queue (green, red). """
        return self.simulation.get_tasks_on_queue()
        
# workflow text related methods

    def current_tasks_as_text(self):
        """ Return the text representing the current tasks. """
        current = self.current_tasks()
        if current is None: return ""
        green, red = current
        text = self.tasks_to_text(green, self.context)
        return text

    def next_tasks_as_text(self):
        """ Return the text representing the next task. """
        next = self.next_tasks()
        if next is None: return ""
        green, red = next
        text = self.tasks_to_text(green, deepcopy(self.context))
        return text

    def tasks_to_text(self, tasks, context):
        """ Convert a list of tasks into one or more sentences. """
        get_log().info('tasks_to_text(%s)' % str(tasks))
        msg = tasks_to_msg_spec(tasks)
        get_log().info(msg)
        txt = self.nlg.process_nlg_doc(msg, self.document.ontology, context)
        get_log().info(txt)
        return txt

    def workflow_as_text(self, doc, filter_items):
#        tasks = list(doc.workflow.tasks())
        task_graph = deepcopy(doc.workflow.task_graph)
        levels = qtgraph.realign_graph(task_graph)
        tasks = []
        for _, ts in levels.items():
            tasks.extend(ts)
        if len(filter_items) > 0:
            print(filter_items)
            included = self.filter_inclusive(filter_items)
            included |= self.get_related_tasks(included)
            tasks = [x for x in tasks if x in included]
            print([task.id for task in tasks])
        text = self.tasks_to_text(tasks, Context(doc.ontology))
        return text

# dialog related methods

    def handle_dialog(self, message):
        """ Parse the message by the current dialog and return
        corresponding ansser. 
        
        """
        result = ''
                # show possible tasks:
        if message == 'help':
            text = """
commands:
    possible tasks
    legal tasks
    assert 'rule' | p (e.g., assert A1:==> vehicle_weight_3)
    retract 'rule' | p (e.g., retract A1:==> vehicle_weight_15)
    print kb
    print af
    why in|out|undec p (e.g.,  why out go_to_Perth1)
    why not p (e.g., why not -forecast_old)
    why p (e.g., why traffic_very_slow)
    next 'task'
                """
            return text

        if message == 'possible tasks':
            tmp = self.simulation.get_tasks_on_queue()
            if tmp is None: return 'Workflow finished'
            tasks, _ = tmp
            g, r = self.current_tasks()
            tasks.extend(g[0].flows_into)
            tasks = list(set(tasks))
            if tasks == []:
                result = 'Not tasks possible.'
            else:
                tasks_txt = [self.tasks_to_text([t], deepcopy(self.context))
                                for t in tasks]
                result = '    ' + '\n    '.join(tasks_txt)
            return result

        # show legal tasks:
        elif message == 'legal tasks':
            tmp = self.simulation.get_tasks_on_queue()
            if tmp is None: return 'Workflow finished'
            tasks, _ = tmp
            if len(tasks) == 0:
                result = 'There are no tasks which can be justified by the KB.'
            else:
                tasks_txt = [self.tasks_to_text([t], deepcopy(self.context))
                                for t in tasks]
                result = '    ' + '\n    '.join(tasks_txt)
            return result

        elif message.startswith('next '):
            task_id = message.split(' ')[1]
            task = self.document.workflow.task(task_id)
            res = self.simulation.set_next_task(task)
            if res is None:
                result = 'Task "%s" is not reachable. ' % task_id
            else:
                self.dialog.parse('assert --> %s' % task_id)
                result = 'Next task changed to %s' % task_id
            return result

        elif message == 'save pdf':
            self.dialog.parse('save')
            return 'Argumentation Framework saved as PDF in /tmp/graph.pdf'
        else:
            resp = self.dialog.parse(message)
            result = self.dialog_response_to_text(resp)
            if self.simulation and not self.simulation.is_finished():
                self.simulation.recalculate()
        return result


# NLG related methods - deprecated

    def lexicalise(self, plan):
        lex = self.nlg.lexicalise(plan)
        return lex
    
    def gre(self, messages, plan):
        lex = self.nlg.gre(messages, plan)
        return lex
    
    def synt_aggregation(self, messages):
        lex = self.nlg.synt_aggregation(messages)
        return lex
    
    def realise_plan(self, original_plan):
        plan = self.hla(original_plan)
        lex = self.nlg.lexicalise(plan)
        lex = self.nlg.gre(lex, plan)
        lex = self.nlg.synt_aggregation(lex)
        return self.nlg.messages_to_text(lex)

    def messages_to_text(self, messages):
        return self.nlg.messages_to_text(messages)

    def document_to_text(self, doc):
        return self.nlg.process_nlg_doc(doc, self.document.ontology)

    def filter(self, objects):
        """ Return tasks that use the given objects. """
        objs = []
        typs = []
        for o in objects:
            if o.startswith(':'): typs.append(o)
            else: objs.append(o)
        obj_tasks = self.document.workflow.tasks_with_objects(objs)
        typ_tasks = self.document.workflow.tasks_with_types(typs)
        return (obj_tasks | typ_tasks)
        
    def filter_inclusive(self, objects):
        """ Return tasks that use the given objects. """
        objs = []
        typs = []
        for o in objects:
            if o.startswith(':'): typs.append(o)
            else: objs.append(o)
        obj_tasks = self.document.workflow.tasks_with_objects_inclusive(objs)
        typ_tasks = self.document.workflow.tasks_with_types_inclusive(typs)
        return (obj_tasks | typ_tasks)

    def get_related_tasks(self, tasks):
        """ Return taks related by AND joins to the given tasks. """
        result = set()
        for t in tasks:
            result |= self.document.workflow.prerequisites(t)
        # also add dependencies for the added objects
        size = -1
        while size != len(result):
            size = len(result)
            for t in list(result):
                result |= self.document.workflow.prerequisites(t)
        return result

    def get_all_tasks(self):
        """ Return a set of all tasks. """
        return set(self.document.workflow.tasks())

    def get_start_and_end(self):
        return self.document.workflow.start_and_end()

    def get_start(self):
        return self.document.workflow.start()

    def get_end(self):
        return self.document.workflow.end()

    def get_arguments(self):
        return list(self.dialog.aaf.arguments)

################# Dialog Response ################

    def dialog_response_to_text(self, response):
        result = None
        if len(response) == 1:
            get_log().warning('Dialog response has more only 1 param:'
                              + repr(response))
            return str(response)
        if len(response) > 2:
            get_log().warning('Dialog response has more than 2 params:'
                              + repr(response))
            if isinstance(response, list):
                if (response[0] == 'The argument' and 
                    response[2] == 'is labeled'):
                    if response[3] == 'in':
                        return 'The task "%s" is possible.' % response[1]
                    elif response[3] == 'out':
                        return 'The task "%s" is not possible.' % response[1]
                    elif response[3] == 'undec':
                        return 'Cannot decide on the task "%s".' % response[1]
                    else:
                        return ' '.join(map(str, response))
                else:
                    return ' '.join(map(str, response))
            return str(response)
        try:
            message, parameter = response
            if 'justification' == message:
                text = self.realise_rule(parameter)
                text = text[0].upper() + text[1:]
                result = 'SYSTEM> ' + text + '.\n'
            else:
                text = self.realise_literal(parameter.consequent)
                text = text[0].upper() + text[1:]
                result = 'SYSTEM> ' + text
                reasoning = self.realise_antecedents(parameter.antecedent)
                if reasoning != '':
                    result += ' because ' + reasoning
                result += '.\n'
        except Exception as e:
            get_log().exception('Exception %s' % str(e))
            get_log().exception('response: %s' % repr(response))
            result = str(response)
        return result

    # TODO: add vulnerabilities to defeasible rules when they undercut the rule
    def realise_rule(self, rule):
        result = self.realise_literal(rule.consequent)
        if len(rule.antecedent) > 0:
            result += ' because '
            result += self.realise_antecedents(rule.antecedent)
        else:
            if isinstance(rule, StrictRule):
                result = ('According to the knowledge base I know that ' +
                          result + ' is true')
            else: # defeasible rule
                result = ('According to the knowledge base I assume that ' +
                          result + ' is true')
        return result

    def realise_literal(self, lit):
        msg = create_literal_msg(lit)
        text = self.nlg.process_nlg_doc(msg, self.document.ontology)
        if lit.negated:
            text = 'It is not the case that ' + text[0].lower() + text[1:]
        return text

    def realise_antecedents(self, antecedent):
        result = ''
        if antecedent == []: return result
        reasons = [self.realise_literal(x) for x in antecedent]
        print(reasons)
        if len(reasons) >=2:
            last_two = reasons[-2:]
            result += ', '.join(reasons[2:])
            result += last_two[0] + ' and ' + last_two[1]
        else:
            result += reasons[0]
        return result


#############################################################################
##
## Copyright (C) 2013 Roman Kutlak, University of Aberdeen.
## All rights reserved.
##
## This file is part of SAsSy Demonstrator program.
##
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of University of Aberdeen nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################
