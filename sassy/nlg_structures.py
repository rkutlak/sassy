from nlg.structures import MsgSpec, Message, Paragraph, Section, Document
from nlg.structures import Word, String, PlaceHolder, Coordination
from nlg.macroplanning import StringMsgSpec

# define a template message spec for a Task

class TaskMsgSpec(MsgSpec):
    """ This class is used for instantiating Tasks as message specifications."""

    def __init__(self, task):
        """ Keep a reference to the corresponding task so that we can look up
        arguments for any variables.
        
        """
        super().__init__(task.name)
        self.task = task

    @classmethod
    def instantiate(Klass, task):
        """ The MsgSpec class relies on instantiate() to create its instances.
        The main reason is so that when the MsgSpec does not have enough data
        to instantiate the class, it just returns None instead of rising 
        an exception.

        """
        if task is None: return None
        c = Klass(task)
        return c

    def value_for(self, param_idx):
        """ When a placeholder needs to be replaced, replace it with
        the input parameter corresponding to the index.
        
        """
        return PlaceHolder(param_idx, self.task.input_params[param_idx])


def task_to_msg_spec(task):
    """ Instantiate a MsgSpec from a given task. """
    return TaskMsgSpec.instantiate(task)


def tasks_to_msg_spec(tasks):
    """ Instantiate MsgSpecs from given tasks and put them into a message. """
    if tasks is None: return None
#    print('Tasks: %d ' % len(tasks))
    msgs = [TaskMsgSpec.instantiate(x) for x in tasks if x is not None]
#    print('Messages: %d ' % len(tasks))
    if len(tasks) > 0:
        return Paragraph(Message('List', None, *msgs))
    else:
        return StringMsgSpec('No tasks selected.')


def describe_workflow(workflow):
    """ Create a summary of a workflow. """
    pass


################################################################################

class LiteralMsgSpec(MsgSpec):
    """ Message specification for literals. """

    def __init__(self, literal):
        # just use a template that returns String() with a placeholder for val
        super().__init__(literal.name)
        self.val = literal

    @classmethod
    def instantiate(Klass, task):
        if task is None: return None
        c = Klass(task)
        return c

    def value_for(self, _):
        return String(self.val.name)

    def features(self):
        ft = { 'NEGATION', self.val.negated }
        return ft


def create_literal_msg(lit):
    return LiteralMsgSpec(lit)

def create_antecedent_msg(antecedent):
    if antecedent == []: return None
    reasons = [LiteralMsgSpec.instantiate(x) for x in antecedent]
    result = Message('Set', None, *reasons)
    return result

def create_rule_msg(rule):
    nucl = LiteralMsgSpec(rule.consequent)
    sat  = create_antecedent_msg(rule.antecedent)
    rel = 'because'
    return Message(rel, nucl, sat)

# TODO: add vulnerabilities to defeasible rules when they undercut the rule

def create_dialog_msg_msg(msg):
    rule = create_rule_msg(parameter)
    result = Message('Leaf', String('SYSTEM>'), rule)
    return result


################################################################################
# define individual messages


class SummariseNumTasks(MsgSpec):
    def __init__(self):
        super().__init__('SummariseNumTasks')
        self.num_tasks = 0

    def arg_num_steps(self):
        return Word(str(self.num_tasks), 'NUMERAL')

    @classmethod
    def instantiate(Klass, workflow):
        if workflow is None: return None
        c = Klass()
        c.num_tasks = len(list(workflow.tasks()))
        return c

    def __repr__(self):
        return('%s : num tasks=%d' % (super().__repr__(), self.num_tasks))

class SummariseImportantTasks(MsgSpec):
    def __init__(self):
        super().__init__('SummariseImportantTasks')

    @classmethod
    def instantiate(self, workflow):
        if workflow is None: return None
        return None


class SummariseNumChoices(MsgSpec):
    def __init__(self):
        super().__init__('SummariseNumChoices')
        self.num_choices = 0

    def arg_num_choices(self):
        return Word(str(self.num_choices), 'NUMERAL')

    @classmethod
    def instantiate(Klass, workflow):
        if workflow is None: return None
        c = Klass()
        c.num_choices = 0
        for t in workflow.tasks():
            if len(t.flows_into) > 1 and t.split_type == 'xor':
                c.num_choices += 1
        return c


class SummariseImportantChoices(MsgSpec):
    def __init__(self):
        super().__init__('SummariseImportantChoices')

    @classmethod
    def instantiate(Klass, workflow):
        if workflow is None: return None
        return None


class SummariseTaskTypes(MsgSpec):
    def __init__(self):
        super().__init__('SummariseTaskTypes')

    @classmethod
    def instantiate(Klass, workflow):
        if workflow is None: return None
        return None


class SummariseTaskLengths(MsgSpec):
    def __init__(self):
        super().__init__('SummariseTaskLengths')

    @classmethod
    def instantiate(Klass, workflow):
        if workflow is None: return None
        return None


messages = [SummariseNumTasks, SummariseImportantTasks, SummariseNumChoices,
            SummariseImportantChoices, SummariseTaskTypes, SummariseTaskLengths]


# in order to structure the selected messages, use 'schemas' - manual structure


def summarise_workflow(workflow):
    p1 = summarise_tasks(workflow)
    p2 = summarise_choices(workflow)
    d = Document('Workflow Summary', Section('', p1, p2))
    return d

def summarise_tasks(workflow):
    m1 = SummariseNumTasks.instantiate(workflow)
    m2 = SummariseImportantTasks.instantiate(workflow)
    m3 = SummariseTaskTypes.instantiate(workflow)
    m4 = SummariseTaskLengths.instantiate(workflow)
    msg = Message('Elaboration', m1, Message('Set', None, m2, m3, m4))
    return Paragraph(msg)


def summarise_choices(workflow):
    m1 = SummariseNumChoices.instantiate(workflow)
    m2 = SummariseImportantChoices.instantiate(workflow)
    msg = Message('Elaboration', m1, m2)
    return Paragraph(msg)



##################################### PDDL #####################################


class SignatureError(Exception):
    pass

class PredicateMsgSpec(MsgSpec):
    """ This class is used for instantiating Predicate as message specifications.
    
    """

    def __init__(self, pred):
        """ Keep a reference to the corresponding predicate so that
        we can look up arguments for any variables.
        
        """
        super().__init__(pred.name)
        self.predicate = pred

    @classmethod
    def instantiate(Klass, task):
        """ The MsgSpec class relies on instantiate() to create its instances.
        The main reason is so that when the MsgSpec does not have enough data
        to instantiate the class, it just returns None instead of rising 
        an exception.

        """
        if task is None: return None
        c = Klass(task)
        return c

    def value_for(self, param):
        """ Return a replacement for a placeholder with id param.
        Predicates have two types of parameters - type_N and var_N, which
        correspond to the type and variable on position N respectively 
        (e.g., ?pkg - package).
        The function returns the type for type_N and the name of the variable
        for var_N at position N or throws SignatureError.

        """
        tmp = param.partition('_')
        idx = -1
        try:
            idx = int(tmp[2])
        except Exception as e:
            msg = 'Expected var_N or type_N in replacing a PlaceHolder ' \
            'but received ' + str(param)
            raise SignatureError(msg)

        if idx >= len(self.predicate.signature):
            msg = 'Requested index (' + str(idx) + ') larger than '\
            'number of variables in predicate "' + str(self.predicate) + '"'
            raise SignatureError(msg)

        parameter = self.predicate.signature[idx]
        result = None
        if tmp[0] == 'type':
            result = Word(parameter[1][0], 'NOUN')
        elif tmp[0] == 'var':
            result = Word(parameter[0], 'NOUN')
        else:
            msg = 'Expected var_N or type_N in replacing a PlaceHolder ' \
            'but received ' + str(param)
            raise SignatureError(msg)

        return result
        

def create_predicate_msg(predicate):
    return PredicateMsgSpec.instantiate(predicate)




















