import unittest

import sys

sys.path.append('aima')
sys.path.append('sassy')
sys.path.append('sassynlg')
sys.path.append('sassygraph')
sys.path.append('sassyargumentation')


from sassy.formats.pddl import pddl
from sassy.dstruct import count_goals
from sassy.nlg_structures import *
import nlg.nlg as nlg
from aima.logic import Expr, expr
from nlg.structures import *
import nlg.lexicalisation as lex


in_pred = Clause(NP(PlaceHolder('type_0'), compl=PlaceHolder('var_0')),
                 VP(Word('is', 'VERB'), PP(Word('in', 'PREPOSITION'),
                             NP(PlaceHolder('type_1'),
                                compl=PlaceHolder('var_1')))))
at_pred = Clause(NP(PlaceHolder('type_0'), compl=PlaceHolder('var_0')),
                 VP(Word('is', 'VERB'), PP(Word('at', 'PREPOSITION'),
                             NP(PlaceHolder('type_1'),
                                compl=PlaceHolder('var_1')))))
in_city_pred = Clause(NP(PlaceHolder('type_0'), compl=PlaceHolder('var_0')),
                      VP(Word('is', 'VERB'), PP(Word('in', 'PREPOSITION'),
                                  NP(PlaceHolder('type_1'),
                                     compl=PlaceHolder('var_1')))))

bad_in_pred = Clause(NP(PlaceHolder('tuple_0'), compl=PlaceHolder('var_0')),
                     VP('is', PP(Word('in', 'PREPOSITION'),
                                 NP(PlaceHolder('type_1'),
                                    compl=PlaceHolder('var_1')))))

lex.add_template('in', in_pred)
lex.add_template('at', at_pred)
lex.add_template('in_city', in_city_pred)
lex.add_template('bad_in', bad_in_pred)


class TestPddlLexicalisation(unittest.TestCase):

    def test_predicate_realisation(self):
        gen = nlg.Nlg()
        p = pddl.Predicate('in', [ ('?pkg', ['package']),
                                   ('?veh', ['vehicle']) ])
        msg = create_predicate_msg(p)
        text = gen.process_nlg_doc(msg, None, None)
        expected = 'package ?pkg is in vehicle ?veh'
        self.assertEqual(expected, text)

    def test_predicate_realisation_exc(self):
        gen = nlg.Nlg()
        # predicate withou enough params for the specified template
        p = pddl.Predicate('in', [ ('?pkg', ['package'])])
        msg = create_predicate_msg(p)
        self.assertRaises(SignatureError,
            lambda : gen.process_nlg_doc(msg, None, None))

        bad_p = pddl.Predicate('bad_in', [ ('?pkg', ['package']),
                                           ('?veh', ['vehicle']) ])
        msg = create_predicate_msg(bad_p)
        self.assertRaises(SignatureError,
            lambda : gen.process_nlg_doc(msg, None, None))

    def test_neg_predicate_realisation(self):
        gen = nlg.Nlg()
        p = pddl.Predicate('in', [ ('?pkg', ['package']),
                                   ('?veh', ['vehicle']) ])
        msg = create_predicate_msg(p)
        msg._features['NEGATED'] = 'true'
        text = gen.process_nlg_doc2(msg, None, None)
        expected = 'Package ?pkg is not in vehicle ?veh.'
        self.assertEqual(expected, text)


class TestGoalDescriptionFunctions(unittest.TestCase):

    def test_count_goals(self):
        gd = expr('A')
        count = count_goals(gd)
        expected = 1
        self.assertEqual(expected, count)
        
        gd = expr('A | B')
        count = count_goals(gd)
        expected = 2
        self.assertEqual(expected, count)
        
        gd = expr('A & (B | C)')
        count = count_goals(gd)
        expected = 2
        self.assertEqual(expected, count)

        gd = expr('(A | ((A & B) & ((A & C) | (C & D))))')
        count = count_goals(gd)
        expected = 3
        self.assertEqual(expected, count)

        gd = expr('(A | B) & (B | C)')
        count = count_goals(gd)
        expected = 4
        self.assertEqual(expected, count)








