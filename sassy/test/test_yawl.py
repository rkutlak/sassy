#import unittest
#import sys
#
#folder = '../src'
#
#if folder not in sys.path:
#    sys.path.append(folder)
#
#
#import formats.yawl as yawl
#import formats.yawl as yawl_gen
#from formats.strips.stripsplan_parser import parse_plan_for_yawl
#from dstruct import Workflow
#
#
#class YawlParserTest(unittest.TestCase):
#    """ Test the parsing of a YAWL file. """
#    def setUp(self):
#        self.filename = '/Users/roman/Work/Sassy/data/credit.yawl'
#
#    def test_parse_file(self):
#        data = yawl.parse_yawl_file(self.filename)
#        self.assertNotEqual(0, len(data.tasks))
#        self.assertNotEqual(0, len(data.decs))
#
#        t1 = data.tasks['recieve_app']
#        self.assertEqual('and', t1.split)
#        self.assertEqual('xor', t1.join)
#
#    def test_workflow_construction(self):
#        workflow = Workflow.from_yawl(self.filename)
#        self.assertEqual('New_Net_1', workflow.name)
#        self.assertEqual('roman', workflow.creator)
#        self.assertEqual('credit', workflow.domain)
#        self.assertEqual('No description has been given.', workflow.description)
#
#
#class YawlGeneratorTest(unittest.TestCase):
#    """ Test generating YAWL from STRIPS plan. """
#    def setUp(self):
#        self.plans_filename = '/Users/roman/Work/Sassy/data/plans.strips'
#        self.output_filename = '/tmp/plan.yawl'
#        with open(self.plans_filename, "r", encoding="utf-8") as f:
#            self.plan_string = f.readline()
#        self.tasks = parse_plan_for_yawl('', self.plan_string)
#
#    def test_parsing(self):
#        self.tasks = parse_plan_for_yawl('', self.plan_string)
#        self.assertNotEqual(0, len(self.tasks))
#
#
#    def test_generation(self):
#        yawl_gen.generate_yawl(self.tasks, self.output_filename)
#
#
## if the module is loaded on its own, run the test
#if __name__ == '__main__':
#    unittest.main()
