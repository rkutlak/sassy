import unittest

import sys

sys.path.append('sassy')
sys.path.append('sassynlg')
sys.path.append('sassygraph')
sys.path.append('sassyargumentation')


from sassy.dstruct import Task, Workflow
from graph.qtgraph import Graph

from nlg.ontology import Ontology

G = {'A': ['B', 'C'],
     'B': ['C', 'D'],
     'C': ['D'],
     'D': ['C'],
     'E': ['F'],
     'F': ['C']}


class TestGraph(unittest.TestCase):
    """ Test basic graph functionality. """

    def test_ctor(self):
        g = Graph(G)
        self.assertEqual({'B', 'C'}, g['A'])
        self.assertEqual({'C', 'D'}, g['B'])
        self.assertEqual({'D'}, g['C'])

        self.assertEqual(6, len(g.nodes))
        self.assertEqual(8, len(g.edges))

    def test_bredth_first(self):
        g = Graph(G)
        # ABCD or ACBD
        self.assertEqual({'A', 'C', 'B', 'D'},
                         set(g.breadth_first_nodes('A')))
        self.assertEqual({'C', 'D'},
                         set(g.breadth_first_nodes('C')))

    def test_depth_first(self):
        g = Graph(G)
        nodes = g.depth_first_nodes('A')
        self.assertEqual({'A', 'B', 'C', 'D'}, set(nodes))
        self.assertEqual(['C', 'D'],
                         list(g.depth_first_nodes('C')))

    def test_find_path(self):
        g = Graph(G)
        path = g.find_path('A', 'D')
        if path != ['A', 'B', 'D'] or path != ['A', 'C', 'D']:
            self.assertEqual(['A', 'B', 'C', 'D'], path)
        self.assertEqual(['A'], g.find_path('A', 'A'))
        self.assertEqual(None, g.find_path('A', 'E'))

    def test_find_all_paths(self):
        g = Graph(G)
        self.assertEqual(sorted([['A', 'B', 'C', 'D'],
                                 ['A', 'B', 'D'],
                                 ['A', 'C', 'D']
                                ]),
                         sorted(g.find_all_paths('A', 'D')))
        self.assertEqual([['A']], g.find_all_paths('A', 'A'))
        self.assertEqual([], g.find_all_paths('A', 'E'))

    def test_shortest_path(self):
        g = Graph(G)
        path = g.find_shortest_path('A', 'D')
        if path != ['A', 'B', 'D']:
            self.assertEqual(['A', 'C', 'D'], path)
        else:
            self.assertEqual(['A', 'B', 'D'], path)

        self.assertEqual(['A'], g.find_path('A', 'A'))
        self.assertEqual(None, g.find_path('A', 'E'))


def helper_create_tasks():
    """ Create a few tasks and arrange them into a workflow.
    Return the workflow. 
    
    """
    t1 = Task('a', 'task a')
    t2 = Task('b', 'task b')
    t3 = Task('c', 'task c')
    t4 = Task('d', 'task d')
    t1.is_input_condition = True
    t4.is_output_condition = True
    t1.flows_into.append(t2)
    t1.flows_into.append(t3)
    t2.flows_into.append(t4)
    t3.flows_into.append(t4)
    t2.join_type = 'AND'
    t3.join_type = 'AND'
    t4.join_type = 'AND'
    tasks = {'a': t1,
             'b': t2,
             'c': t3,
             'd': t4}
    return tasks


class TestWorkflow(unittest.TestCase):

    def test_ctor(self):
        tasks = helper_create_tasks()
        w = Workflow('a test workflow', tasks)
        # every task has to be in both graphs
        for k, v in w.reversed_task_graph.items():
            self.assertEqual(True, k in w.task_graph)

    def test_tasks_with_object(self):
        """ Test whether a workflow correctly returns tasks with an object. """
        tasks = helper_create_tasks()
        tasks['a'].input_params = ['obj1', 'obj2']
        w = Workflow('test', tasks)
        result = w.tasks_with_object('obj1')
        expected = {tasks['a']} # set containing x
        self.assertEqual(expected, result)

    def test_tasks_with_type(self):
        """ Test whether a workflow correctly returns tasks with objects
        of a given type. 
        
        """
        tasks = helper_create_tasks()
        tasks['a'].input_params = ['obj1', 'obj2']
        o = Ontology('sassy/test/data/test.ttl')
        w = Workflow('test', tasks, ontology=o)
        result = w.tasks_with_type(':furniture')
        expected = {tasks['a']} # set containing x
        self.assertEqual(expected, result)

    def test_prerequisites(self):
        """ Test finding pre-requisites for a given action. """
        tasks = helper_create_tasks()
        w = Workflow('test', tasks)
        expected = set()
        result = w.prerequisites(tasks['a'])
        self.assertEqual(expected, result)

        expected = {tasks['a']}
        result = w.prerequisites(tasks['b'])
        self.assertEqual(expected, result)

        expected = {tasks['a']}
        result = w.prerequisites(tasks['c'])
        self.assertEqual(expected, result)

        expected = {tasks['b'], tasks['c']}
        result = w.prerequisites(tasks['d'])
        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()


















