# Convention:
#
# - HSP: hydrostatic pressure
# - BHP: bottom hole pressure
# - ROT: rate of tripping
# - ROM: rate of mud being pumped into a well
# - ROT_greater_ROM : ROT is more than ROM



# possible actions
#
# Drill
# Trip   - removing and re-inserting the complete drill string
# 
# AdjustHSP
# Monitor
#
# SoftShutIn
# HardShutIn
#
# RecordData # collect information for kill sheet
# 
# WaitAndWeight
# DrillersMethod
# ReverseCirculation
# Bullheading
#
# ReopenWell
# SealWell
# PlugWell
#
# Finish



##### epistemic reasoning should establish what propositions are true

### 1. Kick detection

increased_annulus_rate ==> kick
increased_drilling_rate ==> kick
gain_in_pits ==> kick
increased_pump_speed ==> kick
decreased_pump_pressure ==> kick

# swabbing can occur during trip
do_trip, ROT_greater_ROM ==> swabbing

swabbing ==> BHP_low

# 1.3 Lost circulation usually leads to low HSP, which can lead to a kick

mud_weight_low ==> HSP_low

seepage ==> HSP_low

minor_losses ==> HSP_low

sever_losses ==> HSP_low
sever_losses ==> HSP_very_low

# manage lost circulation

HSP_low ==> can_increase_mud_rate
HSP_low ==> can_increase_mud_weight
HSP_low ==> can_cement

BHP_low ==> can_increase_mud_weight

# kick detection behaviour

BHP_low --> alarm

BHP_low, FP ==> BHP_below_FP

BHP_below_FP --> kick

HSP_low --> alarm

HSP_very_low --> kick

kick --> do_shut_in

do_shut_in ==> can_soft_shut_in
do_shut_in ==> can_hard_shut_in

well_shut ==> do_kill

do_kill ==> can_wait_and_weight     # slow, low annulus pressure
do_kill ==> can_drillers_method     # higher annulus pressure than W&W
do_kill ==> can_reverse_circulation # damages tubing, ok when doing workover
do_kill ==> can_bullheading         # quickest, risk of fracturing



# we can always plug the well
--> can_plug
--> can_seal_well
--> can_monitor

drill_bit_blunt ==> do_trip

do_trip =(-alarm, -kick)=> can_trip

=(-alarm, -kick)=> can_drill_and_monitor

=(-do_trip, -alarm, -kick)=> can_finish

alarm ==> can_adjust_HSP

kick_killed --> can_reopen_well
can_reopen_well --> can_dril

##### preference should choose which action to attempt next

can_drill_and_monitor =(-can_trip)=> Drill

can_trip ==> Trip

can_adjust_HSP =(-do_shut_in)=> AdjustHSP

can_monitor ==> Monitor

shallow_depth, kick ==> need_speed

can_soft_shut_in =(-need_speed)=> SoftShutIn
can_hard_shut_in =(-SoftShutIn)=> HardShutIn # prefer soft shut-in

can_record_data ==> RecordData
RecordData ==> RecordData_ask_data

can_wait_and_weight ==> WaitAndWeight
can_drillers_method =(-shallow_depth)=> DrillersMethod
can_workover, can_reverse_circulation ==> ReverseCirculation
can_bullheading =(-HSP_very_low)=> Bullheading

can_reopen_well ==> ReopenWell


can_seal_well =(-can_reopen_well)=> SealWell
can_plug_well =(-can_reopen_well)=> PlugWell

=(can_finish)=> Finish


##### assertions / assumptions

==> shallow_depth


### workflow execution helpers:

#step_5 ==> drill_bit_blunt
#step_6 ==> drill_bit_blunt
#step_7 ==> drill_bit_blunt




