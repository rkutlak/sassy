# assume all actions are allowed

--> inputCondition
==> outputCondition

==> fly_airplane_apn1_apt2_apt1
==> load_truck_obj11_tru1_pos1
==> load_truck_obj12_tru1_pos1
==> load_truck_obj13_tru1_pos1
==> drive_truck_tru1_pos1_apt1_cit1
==> unload_truck_obj13_tru1_apt1
==> unload_truck_obj12_tru1_apt1
==> unload_truck_obj11_tru1_apt1
==> load_airplane_obj12_apn1_apt1
==> load_airplane_obj11_apn1_apt1
==> fly_airplane_apn1_apt1_apt2
==> unload_airplane_obj12_apn1_apt2
==> unload_airplane_obj11_apn1_apt2

==> load_truck_obj21_tru2_pos2
==> drive_truck_tru2_pos2_apt2_cit2
==> unload_truck_obj21_tru2_apt2
==> load_truck_obj11_tru2_apt2
==> drive_truck_tru2_apt2_pos2_cit2
==> unload_truck_obj11_tru2_pos2

# normally all roads are open
=(-edinburgh_stirling_not_possible)=> can_Edinburgh_to_Stirling
=(-edinburgh_kincardine_not_possible)=> can_Edinburgh_to_Kincardine
=(-edinburgh_perth_not_possible)=> can_Edinburgh_to_Perth
=(-stirling_perth_not_possible)=> can_Stirling_to_Perth
=(-perth_aberdeen_not_possible)=> can_Perth_to_Aberdeen
#=(-perth_inverness_not_possible)=> can_Perth_to_Inverness
#=(-aberdeen_inverness_not_possible)=> can_Aberdeen_to_Inverness
=(-aberdeen_perth_not_possible)=> can_Aberdeen_to_Perth
#=(-inverness_aberdeen_not_possible)=> can_Inverness_to_Aberdeen
#=(-inverness_perth_not_possible)=> can_Inverness_to_Perth
=(-perth_stirling_not_possible)=> can_Perth_to_Stirling
=(-perth_kincardine_not_possible)=> can_Perth_to_Kincardine
=(-perth_edinburgh_not_possible)=> can_Perth_to_Edinburgh
=(-kincardine_edinburgh_not_possible)=> can_Kincardine_to_Edinburgh
=(-kincardine_perth_not_possible)=> can_Kincardine_to_Perth
=(-stirling_edinburgh_not_possible)=> can_Stirling_to_Edinburgh

forecast_done_yesterday ==> -forecast_old

forecast_high_wind =(-forecast_old)=> edinburgh_bridge_closed
edinburgh_bridge_closed --> edinburgh_perth_not_possible
edinburgh_bridge_closed --> perth_edinburgh_not_possible

forecast_high_snow ==> inverness_perth_not_possible
forecast_high_snow ==> perth_inverness_not_possible

accident ==> traffic_slow
accident_on_bridge ==> traffic_very_slow

traffic_very_slow --> traffic_slow

R1: ==> -stirling_faster
R2: ==> -kincardine_faster
R3: traffic_slow =(-dont_care_slow)=> kincardine_faster
R4: traffic_very_slow =(-dont_care_slow)=> stirling_faster

R1, R2 < R3 < R4

# 
--> kincardie_bridge_10
A1: ==> vehicle_weight_15

kincardie_bridge_10,vehicle_weight_15 --> edinburgh_kincardine_not_possible
kincardie_bridge_10,vehicle_weight_15 --> kincardine_edinburgh_not_possible

# if we can, we go
can_Edinburgh_to_Perth =(-stirling_faster)=> drive_to_Perth
doing_drive_to_Stirling, can_Stirling_to_Perth ==> drive_to_Perth
doing_drive_to_Kincardine, can_Kincardine_to_Perth ==> drive_to_Perth
done_drive_to_Stirling, can_Stirling_to_Perth ==> drive_to_Perth
done_drive_to_Kincardine, can_Kincardine_to_Perth ==> drive_to_Perth
can_Edinburgh_to_Kincardine =(can_Edinburgh_to_Perth)=> drive_to_Kincardine
can_Edinburgh_to_Kincardine, kincardine_faster ==> drive_to_Kincardine
can_Edinburgh_to_Kincardine, kincardine_faster ==> -drive_to_Stirling
can_Edinburgh_to_Stirling =(can_Edinburgh_to_Kincardine,can_Edinburgh_to_Perth)=> drive_to_Stirling
kincardine_faster, can_Kincardine_to_Edinburgh --> kincardine_better
can_Edinburgh_to_Stirling, stirling_faster =(-kincardine_better)=> drive_to_Stirling

doing_drive_to_Kincardine ==> -can_Edinburgh_to_Stirling
done_drive_to_Kincardine ==> -can_Edinburgh_to_Stirling

can_Perth_to_Inverness ==> Inverness1
can_Perth_to_Aberdeen =(-can_Perth_to_Inverness)=> Aberdeen1

can_Inverness_to_Perth ==> Perth2
can_Aberdeen_to_Perth ==> Perth2
can_Inverness_to_Aberdeen =(-can_Inverness_to_Perth)=> Aberdeen2

done_Perth2, can_Perth_to_Edinburgh ==> Edinburgh2
doing_Stirling2, can_Stirling_to_Edinburgh ==> Edinburgh2
doing_Kincardine2, can_Kincardine_to_Edinburgh ==> Edinburgh2
done_Stirling2, can_Stirling_to_Edinburgh ==> Edinburgh2
done_Kincardine2, can_Kincardine_to_Edinburgh ==> Edinburgh2
can_Perth_to_Kincardine, can_Kincardine_to_Edinburgh =(-can_Perth_to_Edinburgh)=> Kincardine2
can_Perth_to_Stirling =(-can_Perth_to_Edinburgh, -can_Kincardine_to_Edinburgh)=> Stirling2

==> outputCondition

==> accident_on_bridge

done_Aberdeen1  ==> forecast_high_wind
A2: done_Aberdeen1  ==> -vehicle_weight_15
A1 < A2

doing_drive_to_Stirling ==> -drive_to_Kincardine
doing_drive_to_Kincardine ==> -drive_to_Stirling
doing_Stirling2 ==> -Kincardine2
doing_Kincardine2 ==> -Stirling2
