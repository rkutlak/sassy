import unittest

import sys

sys.path.append('sassy')
sys.path.append('sassynlg')
sys.path.append('sassygraph')
sys.path.append('sassyargumentation')

from sassy.dstruct import Workflow
from sassy.formats.pddl import pddl
from nlg_structures import *
import nlg.nlg as nlg
import nlg.lexicalisation as lexicaliser
from argumentation import kb


data = 'sassy/test/data/workflows/'

lin_workflow_5 = Workflow.from_yawl(data + 'linear_workflow_len5.yawl')
lin_workflow_6 = Workflow.from_yawl(data + 'linear_workflow_len6.yawl')
and_workflow_5 = Workflow.from_yawl(data + 'and_fork_workflow_len5.yawl')
and_workflow_6 = Workflow.from_yawl(data + 'and_fork_workflow_len6.yawl')
xor_workflow_5 = Workflow.from_yawl(data + 'xor_fork_workflow_len5.yawl')
xor_workflow_6 = Workflow.from_yawl(data + 'xor_fork_workflow_len6.yawl')


class TestMessages(unittest.TestCase):
    """ Test instantiations of various messages. """

    def test_SummariseNumTasks(self):
        res = SummariseNumTasks.instantiate(lin_workflow_5)
        self.assertEqual(5, res.num_tasks)
        res = SummariseNumTasks.instantiate(lin_workflow_6)
        self.assertEqual(6, res.num_tasks)

        res = SummariseNumTasks.instantiate(and_workflow_5)
        self.assertEqual(5, res.num_tasks)
        res = SummariseNumTasks.instantiate(and_workflow_6)
        self.assertEqual(6, res.num_tasks)

        res = SummariseNumTasks.instantiate(xor_workflow_5)
        self.assertEqual(5, res.num_tasks)
        res = SummariseNumTasks.instantiate(xor_workflow_6)
        self.assertEqual(6, res.num_tasks)

    def test_SummariseNumChoices(self):
        res = SummariseNumChoices.instantiate(lin_workflow_5)
        self.assertEqual(0, res.num_choices)
        res = SummariseNumChoices.instantiate(lin_workflow_6)
        self.assertEqual(0, res.num_choices)

        res = SummariseNumChoices.instantiate(and_workflow_5)
        self.assertEqual(0, res.num_choices)
        res = SummariseNumChoices.instantiate(and_workflow_6)
        self.assertEqual(0, res.num_choices)

        res = SummariseNumChoices.instantiate(xor_workflow_5)
        self.assertEqual(1, res.num_choices)
        res = SummariseNumChoices.instantiate(xor_workflow_6)
        self.assertEqual(1, res.num_choices)


class TestSchema(unittest.TestCase):
    """ Test creation of a Document from a workflow using various functions"""

    def test_summarise_choice(self):
        p = summarise_choices(xor_workflow_6)
        print(str(p))
        m = p.messages[0]
        self.assertEqual('Elaboration', m.rst)
        self.assertEqual('SummariseNumChoices', str(m.nucleus))
        self.assertEqual('[]', str(m.satelites))

    def test_summarise_tasks(self):
        p = summarise_tasks(xor_workflow_6)
        print(str(p))
        m = p.messages[0]
        self.assertEqual('Elaboration', m.rst)
        self.assertEqual('SummariseNumTasks', str(m.nucleus))
        self.assertEqual('[Message (Set): _empty_]', str(m.satelites))

    def test_summarise_workflow(self):
        d = summarise_workflow(xor_workflow_6)
        self.assertEqual(True, isinstance(d, Document))
        self.assertEqual('Workflow Summary', d.title)
        self.assertEqual(1, len(d.sections))
        self.assertEqual(2, len(d.sections[0].paragraphs))


class TestLexicalisation(unittest.TestCase):
    """ Test lexicalisation of schemas. """

    def test_lexicalise_steps(self):
        m = summarise_tasks(xor_workflow_6)
        lex = lexicaliser.lexicalise(m)
        self.assertIsNotNone(lex)
        expected = '\tthe workflow has 6 tasks'
        self.assertEqual(expected, str(lex))

    def test_lexicalise_workflow(self):
        """ Test that workflow summary is lexicalised. """
        d = summarise_workflow(xor_workflow_6)
        lex = lexicaliser.lexicalise(d)
        self.assertIsNotNone(lex)
        expected = ('Workflow Summary\n\n' +
                    '\tthe workflow has 6 tasks\n' +
                    '\tthe workflow has 1 choices')

        self.assertEqual(expected, str(lex))

    def test_lexicalise_literal(self):
        """ Test lexicalisation of KB literals. """
        l = kb.Literal.from_str('traffic_very_slow')
        m = LiteralMsgSpec(l)
        gen = nlg.Nlg()
        text = gen.process_nlg_doc(m, None, None)
        expected = 'the traffic is very slow'
        self.assertEqual(expected, text)

    def test_lexicalise_antecedent(self):
        l1 = kb.Literal.from_str('traffic_very_slow')
        l2 = kb.Literal.from_str('edinburgh_bridge_closed')
        m = create_antecedent_msg([l1,l2])
        gen = nlg.Nlg()
        text = gen.process_nlg_doc(m, None, None)
        expected = 'The traffic is very slow. ' + \
                   'Forth Road Bridge outside Edinburgh is closed.'
        self.assertEqual(expected, text)


    def test_lexicalise_rule(self):
        s = 'accident_on_bridge ==> traffic_very_slow'
        m = create_rule_msg(kb.DefeasibleRule.from_str(s))
        gen = nlg.Nlg()
        text = gen.process_nlg_doc(m, None, None)
        expected = 'The traffic is very slow because ' + \
                   'of an accident on the bridge.'
        self.assertEqual(expected, text)










