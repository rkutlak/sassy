Coding style should follow Python Enhancement Proposals (PEP) 8
http://www.python.org/dev/peps/pep-0008/

here are a few points:
  - max 80 (preferably max 79) characters per line
  - module names all lowercase, can use underscore
  - package names all lowercase, underscores discouraged
  - separate top-level function and class definitions with two blank lines.
  - method definitions inside a class are separated by a single blank line.
  - extra blank lines may be used to separate groups of related functions.
  
For guide on documenting your code see PEP 257:
    http://www.python.org/dev/peps/pep-0257/

NLG
  - the mapping from actions to sentences is domain dependent and should be
    read from DB or something similar.

  - in order to create basic REs that contain types, the domain has to have
    information about the existign objects and their types (problem.pddl)
    
  - probably create a class LanguageResources that can parse pddl domain,
    problem, preferences, mappings, etc.
    
    
Create an internal representation for plan to avoid reading from/to files.

Because the code is organised in packages, it is not always possible
to load a .py file on its own for debugging. Should you want to do that,
include the following at the beginning of the file, before project package
imports:

import sys
sys.path.append('/Users/roman/Work/SAsSy/src/')

The above added the path into the global sys.path. sys.path is searched when
the interpreter looks for modules to import.



converting n3 formated ontology to xml using rdflib:

import rdflib
from rdflib.namespace import Namespace

g = rdflib.Graph()
result = g.parse("logistics.ttl", format="n3")
g.serialize('logistcs.xml', format='xml')


# print stuff
for s, p, o in g:
    print((s, p, o))

# assign a namespace
n = Namespace("http://scrutable-systems.org/ontology/Logistics#")
g.bind("logistics", n)

# simple SPARQL example:
import rdflib

    g = rdflib.Graph()

    # ... add some triples to g somehow ...
g.parse("some_foaf_file.rdf")

qres = g.query(
    """SELECT DISTINCT ?aname ?bname
       WHERE {
          ?a foaf:knows ?b .
          ?a foaf:name ?aname .
          ?b foaf:name ?bname .
       }""")

for row in qres:
    print("%s knows %s" % row)
    