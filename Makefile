
USER=roman
SHELL=/bin/bash
# Note that everyone will have a different path.
#  Regardless of that, it should contain path to the python command and PyQt.
export PATH := /Library/Frameworks/Python.framework/Versions/3.3/bin:$(PATH)

# name of the package
package  = sassy
app      = SassyDemo.app
platform = SassyDemoMac
dest     = /Users/roman/shared_vm_folder
res      = $(package)/resources/
PYTHON_PATH = /Library/Frameworks/Python.framework/Versions/3.3/lib/python3.3/



OPTIONS =  --resources $(res)/log.config.yaml,$(package)/nlg/resources/simplenlg.jar --packages rdflib,logging


# default target - run the app from place
debug: structure
	cd tmp && \
	python3 setup.py py2app -A $(OPTIONS)

release: structure
	rm -rf $(dest)/$(package)/$(platform)
	mkdir $(dest)/$(package)/$(platform) && \
	cd tmp && \
	python3 setup.py py2app $(OPTIONS) && \
	cd dist/$(app)/Contents/ && \
	cp -R /Developer/Qt5.2/5.2.0/clang_64/plugins PlugIns
	macdeployqt tmp/dist/$(app)
	cp $(PYTHON_PATH)/config-3.3m/Makefile tmp/dist/$(app)/Contents/Resources/lib/python3.3/config-3.3m/
	cp -R tmp/dist/$(app) $(dest)/$(package)/$(platform)/$(app)

# windows uses cygwin
windows: structure
	cd tmp && \
	mv setup.py $(package)/
	cd tmp/$(package) && \
	python setup.py build
	cp /cygdrive/e/libEGL.dll tmp/$(package)/build/exe.win32-3.4/
	rm -rf /cygdrive/e/sassy/Windows/SassyDemo
	cp -R tmp/$(package)/build/exe.win32-3.4 /cygdrive/e/$(package)/$(platform)/SassyDemo


examples: 
	rm -rf $(dest)/$(package)/$(platform)/examples
	mkdir $(dest)/$(package)/$(platform)/examples && \
	cp -R tmp/$(package)/test/data/Logistics1.sassy $(dest)/$(package)/$(platform)/examples
	cp -R tmp/$(package)/test/data/Logistics2.sassy $(dest)/$(package)/$(platform)/examples
	cp -R tmp/$(package)/test/data/Logistics3.sassy $(dest)/$(package)/$(platform)/examples


packmac: release examples
	cd $(dest)/$(package)/ && \
	tar -czf $(platform).tar.gz $(platform)


# autogenerate basic setup file -- prefer the one in main package
setup: structure
	cd tmp && \
	py2applet --make-setup $(package)/main.py

# create a new copy of the package
structure: clean
	mkdir tmp
	cp -R sassy tmp/$(package)
	cp -R sassynlg/nlg tmp/$(package)/
	cp -R sassygraph/graph tmp/$(package)/
	cp -R sassyargumentation/argumentation tmp/$(package)/
	cp setup.py tmp/

run:
	open tmp/dist/$(app)

all: release

.PHONY : clean
clean:
	rm -rf tmp


################################################################################

# this target was supposed to check the syntax but it does not seem to work
build:
	python -m py_compile $(source_files)


print_source_files:
	echo $(source_files)

print_bytecode_files:
	echo $(bytecode_files)


# remove precompiled files
#.PHONY : clean
#clean :
#	-rm -rf src/__pycache__
#	-rm -f $(bytecode_files)


# Xcode and make docs: 
# https://developer.apple.com/library/mac/#documentation/DeveloperTools/Reference/XcodeBuildSettingRef/1-Build_Setting_Reference/build_setting_ref.html#//apple_ref/doc/uid/TP40003931-CH3-SW45

