import sys
import logging

from PyQt5.QtWidgets import QApplication

import PyQt5.QtGui as QtGui
import PyQt5.QtCore as QtCore
import PyQt5.QtWidgets as QtWidgets

import autocomplete as ac

MAC = True
try:
    from PyQt5.QtGui import qt_mac_set_native_menubar
except ImportError:
    MAC = False

logging.getLogger(__name__).addHandler(logging.NullHandler())

def get_log():
    return logging.getLogger(__name__)


################################################################################

import re


def completer(text, state):
    res = []
    wordList = ["flyToBase", "flyToAirfieldA", "flyToAirfieldB"]
    if state == 0:
        for word in wordList:
            if word.startswith(text): res.append(word)
    if len(res) == 0: res = None
    return res


class ReadlineCompleter(QtWidgets.QCompleter):

    def __init__(self, keywords, *args, **kwargs):
        super(ReadlineCompleter, self).__init__(*args, **kwargs)
        self.keywords = keywords
        self.matches = None
        self.setModel(QtCore.QStringListModel())
        self.updateModel()

    def setCompletionPrefix(self, val):
        self.matches = None
        super(ReadlineCompleter, self).setCompletionPrefix(val)
        self.updateModel()

    def currentCompletion(self):
        state = self.currentRow()
        return self.matches[state]

#    def completionCount(self):
#        if self.matches is None: return 0
#        return len(self.matches)

    def updateModel(self):
        self.matches = []
        text = str(self.completionPrefix())
#        text = completionText

        tmp = text.rpartition(' ')
        word = tmp[2]
        print('looking for matches for word "%s"' % word)
        for keyword in self.keywords:
            if keyword.startswith(word): self.matches.append(keyword)

        self.model().setStringList(self.matches)


class CustomQCompleter(QtWidgets.QCompleter):
    def __init__(self, parent=None):
        super(CustomQCompleter, self).__init__(parent)
        self.local_completion_prefix = ""
        self.source_model = None

    def setModel(self, model):
        self.source_model = model
        super(CustomQCompleter, self).setModel(self.source_model)

    def updateModel(self):
        local_completion_prefix = self.local_completion_prefix
        class InnerProxyModel(QtCore.QSortFilterProxyModel):
            def filterAcceptsRow(self, sourceRow, sourceParent):
                index0 = self.sourceModel().index(sourceRow, 0, sourceParent)
                return local_completion_prefix.lower() in self.sourceModel().data(index0).lower()
        proxy_model = InnerProxyModel()
        proxy_model.setSourceModel(self.source_model)
        super(CustomQCompleter, self).setModel(proxy_model)

    def splitPath(self, path):
        self.local_completion_prefix = path
        self.updateModel()
        return ""


class CompletedLine(QtWidgets.QLineEdit):
    def __init__(self, parent=None):
        super(CompletedLine, self).__init__(parent)
        self.completer = None

    def setCompleter(self, completer):
        if self.completer:
            self.completer.disconnect()
        if not completer:
            return
        completer.setWidget(self)
        completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.completer = completer
        self.completer.activated.connect(self.insertCompletion)

    def insertCompletion(self, completion):
        prefix = self.text().rpartition(' ')[0]
        self.setText(prefix + ' ' + completion)

    def textUnderCursor(self):
        # return the last word?
        return self.text().rpartition(' ')[2]

    def focusInEvent(self, event):
        if self.completer:
            self.completer.setWidget(self);
        QtWidgets.QLineEdit.focusInEvent(self, event)

    def keyPressEvent(self, event):
        if self.completer and self.completer.popup().isVisible():
            if event.key() in (
            QtCore.Qt.Key_Enter,
            QtCore.Qt.Key_Return,
            QtCore.Qt.Key_Escape,
            QtCore.Qt.Key_Tab,
            QtCore.Qt.Key_Backtab):
                event.ignore()
                return
        ## has ctrl-E been pressed??
        isShortcut = (event.modifiers() == QtCore.Qt.ControlModifier and
                      event.key() == QtCore.Qt.Key_E)
        if (not self.completer or not isShortcut):
            QtWidgets.QLineEdit.keyPressEvent(self, event)

        ## ctrl or shift key on it's own??
        ctrlOrShift = event.modifiers() in (QtCore.Qt.ControlModifier ,
                QtCore.Qt.ShiftModifier)
        if ctrlOrShift and event.text() == '':
            # ctrl or shift key on it's own
            return

        eow = "~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-=" #end of word

        hasModifier = ((event.modifiers() != QtCore.Qt.NoModifier) and
                        not ctrlOrShift)

        completionPrefix = self.textUnderCursor()
        print('prefix: "%s"' % completionPrefix)
        if (not isShortcut and (hasModifier or event.text() == '' or
        len(completionPrefix) < 3 or
        (event.text()[-1] in eow))):
            self.completer.popup().hide()
            return

        if (completionPrefix != self.completer.completionPrefix()):
            self.completer.setCompletionPrefix(completionPrefix)
            popup = self.completer.popup()
            popup.setCurrentIndex(
                self.completer.completionModel().index(0,0))

        self.completer.complete()




class AutocompleteMainWindow(QtWidgets.QMainWindow, ac.Ui_MainWindow):

    def __init__(self, parent=None, f=QtCore.Qt.WindowFlags()):
        QtWidgets.QMainWindow.__init__(self, parent, f)
        # set up the widgets
        self.setupUi(self)

        # autocomplete for dialog
        wordList = ["flyToBase", "flyToAirfieldA", "flyToAirfieldB"]

#        self.horizontalLayout.removeWidget(self.lineEdit)
#        edit = CompletedLine(self)
        self.lineEdit.setCompleter(QtWidgets.QCompleter(wordList))
#        self.horizontalLayout.insertWidget(1, edit)




################################################################################


def run():
    logging.basicConfig(level=logging.WARNING)
    app = QApplication(sys.argv)
    window = AutocompleteMainWindow()
    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    run()



