#---------
# IMPORT
#---------
import sys, random

from PyQt5.QtWidgets import QApplication

import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets

#---------
# DEFINE
#---------
class QDialogTest(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(QDialogTest, self).__init__(parent)

        self.maxVisibleItems = 7

        self.lineEdit = QtWidgets.QLineEdit(self)
        self.lineEdit.textChanged.connect(self.on_lineEdit_textChanged)

        self.standardItemModel = QtGui.QStandardItemModel(self)

#        with open("/usr/share/dict/words", "r") as fileInput:
#            for line in  random.sample(fileInput.readlines(), 111):
#                self.standardItemModel.appendRow(
#                    QtWidgets.QStandardItem(line.strip())
#                )

        for x in ['flyToBase', 'flyToAirfieldA', 'flyToAirfieldB']:
            self.standardItemModel.appendRow(QtGui.QStandardItem(x))

        self.sortFilterProxyModel = QtCore.QSortFilterProxyModel(self)
        self.sortFilterProxyModel.setSourceModel(self.standardItemModel)
        self.sortFilterProxyModel.setFilterKeyColumn(0)

        self.tableView = QtWidgets.QTableView(self)
        self.tableView.horizontalHeader().setStretchLastSection(True)
        self.tableView.setModel(self.sortFilterProxyModel)

        self.pushButtonClose = QtWidgets.QPushButton(self)
        self.pushButtonClose.setText("Close")
        self.pushButtonClose.clicked.connect(sys.exit)

        self.layoutVertical = QtWidgets.QVBoxLayout(self)
        self.layoutVertical.addWidget(self.tableView)
        self.layoutVertical.addWidget(self.lineEdit)
        self.layoutVertical.addWidget(self.pushButtonClose)

        self.tableViewPopup = QtWidgets.QTableView(self)
        self.tableViewPopup.setModel(self.sortFilterProxyModel)
        self.tableViewPopup.setWindowFlags(QtCore.Qt.Popup)
        self.tableViewPopup.setFocusPolicy(QtCore.Qt.NoFocus)
        self.tableViewPopup.setFocusProxy(self.lineEdit)
        self.tableViewPopup.setMouseTracking(True)
        self.tableViewPopup.setEditTriggers(QtWidgets.QTableView.NoEditTriggers)
        self.tableViewPopup.setSelectionBehavior(QtWidgets.QTableView.SelectRows)
        self.tableViewPopup.setFrameStyle(QtWidgets.QFrame.Box | QtWidgets.QFrame.Plain)
        self.tableViewPopup.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.tableViewPopup.horizontalHeader().setStretchLastSection(True)
        self.tableViewPopup.horizontalHeader().hide()
        self.tableViewPopup.verticalHeader().hide()
        self.tableViewPopup.verticalHeader().setDefaultSectionSize(20)
        self.tableViewPopup.doubleClicked.connect(self.setCurrentCompletion)
        self.tableViewPopup.installEventFilter(self)

    def setCurrentCompletion(self):
        self.closePopup()

        indexes = self.tableViewPopup.selectionModel().selectedIndexes()

        self.lineEdit.blockSignals(True)
        self.lineEdit.setText(indexes[0].data(QtCore.Qt.DisplayRole))
        self.lineEdit.blockSignals(False)

    def closePopup(self):
        self.tableViewPopup.hide()
        self.lineEdit.setFocus()

    def eventFilter(self, obj, event):
        if obj != self.tableViewPopup:
            return False

        elif event.type() == QtCore.QEvent.MouseButtonPress:
            self.closePopup()

        elif event.type() == QtCore.QEvent.KeyPress:
            if event.key() in [
                QtCore.Qt.Key_Enter,
                QtCore.Qt.Key_Return
            ]:
                self.setCurrentCompletion()

            elif event.key() in [
                QtCore.Qt.Key_Escape
            ]:
                self.closePopup()

            elif not event.key() in [
                QtCore.Qt.Key_Up,
                QtCore.Qt.Key_Down,
                QtCore.Qt.Key_Home,
                QtCore.Qt.Key_End,
                QtCore.Qt.Key_PageUp,
                QtCore.Qt.Key_PageDown
            ]:
                self.lineEdit.event(event)

        return super(QDialogTest, self).eventFilter(obj, event)

    @QtCore.pyqtSlot(str)
    def on_lineEdit_textChanged(self, text):
        self.tableViewPopup.hide()

        if text != "":
            self.setCompletionPrefix(text)

    def setCompletionPrefix(self, prefix):
        self.sortFilterProxyModel.setFilterRegExp(QtCore.QRegExp(prefix))

        if self.sortFilterProxyModel.rowCount():
            self.complete()

    def complete(self):
        self.tableViewPopup.move(
            self.lineEdit.mapToGlobal(
                QtCore.QPoint(0, self.lineEdit.height())
            )
        )
        self.tableViewPopup.resize(
            self.lineEdit.width(), 
            self.tableViewPopup.verticalHeader().defaultSectionSize() * min(
                self.maxVisibleItems,
                self.sortFilterProxyModel.rowCount()
            ) + 2
        )
        self.tableViewPopup.setFocus()
        self.tableViewPopup.show()       

#---------
# MAIN
#---------
if __name__ == "__main__":    
    app = QApplication(sys.argv)
    app.setApplicationName('QDialogTest')

    main = QDialogTest()
    main.resize(333, 333)
    main.exec_()

    sys.exit(app.exec_())
    